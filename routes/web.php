<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FloorController@index');

Route::post('floor_plan', 'FloorController@store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/file-manager', 'HomeController@filemanager')->name('file_manager');

## test method...
