<?php

use App\AppOptions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// To get the all floors
Route::get(
    '/floors',
    function (Request $request) {
        return \App\Floor::all();
    }
);
// To get the specific floor
Route::get(
    '/floor/{id}',
    function ($id) {
        return \App\Floor::find($id);
    }
);
// To get the last floor
Route::get(
    '/last-floor/',
    function () {
        return [
            'lastFloor'       => \App\Floor::orderBy('id', 'desc')->first(),
            'operatory_rooms' => \App\Http\Controllers\FloorController::coordinatesOfOpsRoom(),
        ];
    }
);
// get the layers.
Route::get(
    '/layers',
    function () {
        return [
            'main_door'       => \App\floorLayers::getLayerByType('main_door'),
            'layers'          => \App\floorLayers::visibleLayers(),
            'images_folder'   => Storage::directories('/public/'),
            'image_base_url'  => \App\Http\Controllers\FloorController::findBaseUrl(),
            'selected_folder' => AppOptions::getOption('selected_image_folder'),
        ];
    }
);

Route::get(
    '/select-image-folder',
    function (Request $request) {
        $Option = \App\AppOptions::where('name', '=', 'selected_image_folder')->first();
        if ( ! $Option) {
            $Option = new \App\AppOptions();
        }
        try {
            $Option->name       = 'selected_image_folder';
            $Option->value      = $request->query->get('folder', 'default');
            $Option->value_type = 'string';
            $Option->save();

            return response(['type' => 'success', 'message' => 'changes saved'])->json();
        } catch (Exception $exception) {
            return response(['type' => 'error', 'message' => $exception->getMessage()])->json();
        }
    }
);


//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
