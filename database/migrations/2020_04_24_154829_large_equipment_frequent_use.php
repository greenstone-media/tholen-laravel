<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LargeEquipmentFrequentUse extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'floors',
            function (Blueprint $table) {
                $table->boolean('is_laser_or_large_equipment_use_frequently');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'floors',
            function (Blueprint $table) {
                $table->dropColumn(['is_laser_or_large_equipment_use_frequently']);
            }
        );
    }
}
