<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DoorsRotationInLayers extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'layer',
            function (Blueprint $table) {
                $table->integer('door_rotation')->default(0);
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'layer',
            function (Blueprint $table) {
                $table->dropColumn(['door_rotation']);
            }
        );
    }
}
