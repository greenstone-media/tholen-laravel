<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CoffeeBar extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'floors',
            function (Blueprint $table) {
                $table->boolean('is_coffee_bar')->default(false);
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'floors',
            function (Blueprint $table) {
                $table->dropColumn(['is_coffee_bar']);
            }
        );
    }
}
