<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDoorToLayer extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'layer',
            function (Blueprint $table) {
                $table->integer('door_x_axis')->default(0);
                $table->integer('door_y_axis')->default(0);
                $table->string('door_picture')->nullable();
                $table->boolean('show_door')->default(false);
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'layer',
            function (Blueprint $table) {
                $table->dropColumn(
                    [
                        'door_x_axis',
                        'door_y_axis',
                        'door_picture',
                        'show_door',
                    ]
                );
            }
        );
    }
}
