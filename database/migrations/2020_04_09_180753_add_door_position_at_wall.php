<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDoorPositionAtWall extends Migration
{

    protected $tableName = 'floors';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            $this->tableName,
            function (Blueprint $table) {
                $table->integer('door_position')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            $this->tableName,
            function (Blueprint $table) {
                $table->dropColumn(['door_position']);
            }
        );
    }
}
