<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFloorsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'floors',
            function (Blueprint $table) {
                $table->id();
                $table->integer('operatory_rooms');
                $table->integer('square_footage');
                $table->string('building_shape');
                $table->string('length');
                $table->string('width');
                $table->boolean('rural_area')->nullable();
                $table->boolean('cbct')->nullable();
                $table->string('type')->nullable();
                $table->integer('front_desk_people')->nullable();
                $table->integer('number_of_staff')->nullable();
                $table->string('display')->nullable();
                $table->string('floorplan')->nullable();
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('floors');
    }
}
