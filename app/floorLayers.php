<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class floorLayers extends Model
{

    //
    protected $table = 'layer';

    public static function visibleLayers()
    {
        return self::where('layer.hide', '=', 0)->get();
    }

    public static function getLayerByType($type = 'main_door')
    {
        return self::where('layer.type', '=', $type)->first();
    }

}
