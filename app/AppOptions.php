<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppOptions extends Model
{

    protected $table = 'options';

    public static function getOption($option_name, $value = true)
    {
        $Option = self::where('name', '=', $option_name)->first();

        if (is_object($Option)) {
            return $Option->value;
        } else {
            return 'default';
        }
    }
}
