<?php

namespace App\Http\Controllers;

use App\AppOptions;
use App\Floor;
use App\floorLayers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\Types\Self_;

class FloorController extends Controller
{

    const doorPictureArray = [
        '0degree'              => 'http://134.122.28.235/img_17_april/Door.png',
        'grayBottomKnoch'      => 'http://134.122.28.235/img_17_april/grayBottomKnoch.png',
        'yellowBottomKnoch'    => 'http://134.122.28.235/img_17_april/yellowBottomKnoch.png',
        'blueBottomKnoch'      => 'http://134.122.28.235/img_17_april/blueBottomKnoch.png',
        'blueUpKnoch'          => 'http://134.122.28.235/img_17_april/blueTopKnoch.png',
        'grayUpKnoch'          => 'http://134.122.28.235/img_17_april/grayUpKnoch.png',
        'darkBlueBottomKnotch' => 'http://134.122.28.235/img_17_april/darkBlueBottomKnotch.png',
        'darkBlueUpKnotch'     => 'http://134.122.28.235/img_17_april/darkBlueUpKnotch.png',

        'blueKnochDownLeft'  => 'http://134.122.28.235/img_17_april/blueKnochDownLeft.png',
        'blueKnochDownRight' => 'http://134.122.28.235/img_17_april/blueKnochDownRight.png',
        'blueKnochLeftDown'  => 'http://134.122.28.235/img_17_april/blueKnochLeftDown.png',
        'blueKnochRightDown' => 'http://134.122.28.235/img_17_april/blueKnochRightDown.png',

        'grayKnochDownLeft'  => 'http://134.122.28.235/img_17_april/grayKnochDownLeft.png',
        'grayKnochDownRight' => 'http://134.122.28.235/img_17_april/grayKnochDownRight.png',
        'grayKnochLeftDown'  => 'http://134.122.28.235/img_17_april/grayKnochLeftDown.png',
        'grayKnochRightDown' => 'http://134.122.28.235/img_17_april/grayKnochRightDown.png',

        'yellowKnochDownLeft'     => 'http://134.122.28.235/img_17_april/yellowKnochDownLeft.png',
        'yellowKnochDownRight'    => 'http://134.122.28.235/img_17_april/yellowKnochDownRight.png',
        'yellowKnochLeftDown'     => 'http://134.122.28.235/img_17_april/yellowKnochLeftDown.png',
        'yellowKnochRightDown'    => 'http://134.122.28.235/img_17_april/yelloKnochRightDown.png',
        'grayKnotchUpLeftOpening' => 'http://134.122.28.235/img_17_april/grayKnotchUpLeftOpening.png',
        'whiteDoorOpenBottom'     => 'http://134.122.28.235/img_17_april/whiteDoorOpenBottom.png',

        '90degree'  => '',
        '180degree' => '',
        '270degree' => '',
        '360degree' => '',
    ];
    const operatoryRoomWidth = 120;
    const doorWidth = 30;
    const doorLength = 30;
    const operatoryRoomActualWidth = 100;
    const operatoryRoomLength = 115;
    public $horizontalHallway = 40;
    public $verticalHallway = 40;
    public $secondVerticalHallway = 40;
    public $operatoryRoomsAtTwoWalls = false;
    public $availableRoomsAtFootPrint = 1;
    public $layersDimensionsAndSize = [];
    public $isPublicZoneUper = false;
    public $isPublicZoneAtRight = false;
    public $horizontalSegmentsInLayer = 0;
    public $horizontalSegmentsLength = 0;
    public $horizontalSegmentsWidth = 0;
    public $staffContainerLength = 0;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function findBaseUrl($filename = null)
    {
        return asset('storage/'.AppOptions::getOption('selected_image_folder')).DIRECTORY_SEPARATOR;
        //        return Storage::url(AppOptions::getOption('selected_image_folder').DIRECTORY_SEPARATOR);


        //        if (Storage::exists(AppOptions::getOption('selected_image_folder').DIRECTORY_SEPARATOR.$filename)) {
        //            return Storage::url(AppOptions::getOption('selected_image_folder').DIRECTORY_SEPARATOR.$filename);
        //        } else {
        //            return asset('img_17_april'.DIRECTORY_SEPARATOR.$filename);
        //        }
    }

    public function index(Request $request)
    {
        $lastFloor = \App\Floor::orderBy('id', 'desc')->first();
        $this->saveLayers($lastFloor);
        //        $layers = [
        //            'locateStaffContainer' => $this->locateStaffContainer($lastFloor),
        //
        //            'availableSpaceOfFootPrint' => $this->findAvailableSpaceAtFootPrint($lastFloor),
        //            'xHallway'                  => $this->locateTreatmentHallway($lastFloor),
        //            'yHallway'                  => $this->locateCorridorHallway($lastFloor),
        //            'secondCorridorHallway'     => $this->locateSecondCorridorHallway($lastFloor),
        //            //            'firstHorizontalSpace'    => $this->findFirstHorizontalSpecification($lastFloor),
        //            //            'publicZoneSpecification' => $this->findPublicZoneSpecification($lastFloor),
        //            //            'xRay'                    => $this->locateXRayAlcove($lastFloor),
        //            //            'mechanicalRoom'          => $this->locateMechanicalRoom($lastFloor),
        //            //            'consultRoom'             => $this->locateConsultRoom($lastFloor),
        //            //            'storage'                 => $this->locateBulkStorage($lastFloor),
        //            //
        //            //
        //            'last_floor'                => $lastFloor,
        //            //            'operatoryRoomsLocation'    => $this->findOperatoryRoomPositionAndNumber($lastFloor),
        //            //            'doorSpecification'         => $this->findFrontDoorSpecification($lastFloor),
        //            //            'privateZoneSpecification'  => $this->findPrivateZoneSpecification($lastFloor),
        //            //            'clinicalZoneSpecification' => $this->findClinicalZoneSpecification($lastFloor),
        //            //            'sterilization'             => $this->chooseSterilization($lastFloor),
        //            //            'doctorPrivateOffice'       => $this->locateDoctorPrivateOffice($lastFloor),
        //            //            'doctorToilet'              => $this->locateDoctorToilet($lastFloor),
        //            //            'patientToilet'             => $this->locatePatientToilet($lastFloor),
        //            //            'waitingRoom'               => $this->locateWaitingRoom($lastFloor),
        //            //            'receptionRoom'             => $this->locateReception(),
        //            //
        //            //            'staffLounge'         => $this->locateStaffLounge($lastFloor),
        //            //            'staffToilet'         => $this->locateStaffToilet($lastFloor),
        //            //            'lab'                 => $this->labSpecification($lastFloor),
        //            //            'doctorOfficeConflue' => $this->locateDoctorOfficeConfluence($lastFloor),
        //            //            'locateSemi'          => $this->locateSemiprivate($lastFloor),
        //            //            'locateCompasLayer'   => $this->locateCompasLayer($lastFloor),
        //
        //        ];
        //
        //        if ($lastFloor->is_coffee_bar) {
        //            $layers['coffee_bar'] = $this->locateCoffeeBar($lastFloor);
        //        }

        $layers               = $this->layersList($lastFloor);
        $layers['last_floor'] = $lastFloor;


        return view(
            'welcome',
            $layers
        );
    }

    public function store(Request $request)
    {

        $this->validate(
            $request,
            [
                'operatory_rooms' => 'required|numeric|min:1',
                'square_footage'  => 'required',
            ]
        );
        $floor                                             = new Floor();
        $floor->operatory_rooms                            = $request->operatory_rooms;
        $floor->frontdoor_facing                           = $request->frontdoor_facing;
        $floor->square_footage                             = $request->square_footage;
        $floor->building_shape                             = ($request->building_shape) ? $request->building_shape : 50;
        $floor->rural_area                                 = ($request->office_rural) == 'on' ? 1 : 0;
        $floor->cbct                                       = ($request->radiography_used) == 'on' ? 1 : 0;
        $floor->is_coffee_bar                              = ($request->is_coffe_bar) == 'on' ? true : false;
        $floor->is_laser_or_large_equipment_use_frequently = ($request->is_laser_or_larger_equipment) == 'on' ? true : false;
        $floor->type                                       = $request->type_office;
        $floor->front_desk_people                          = $request->front_desk_people;
        $floor->number_of_staff                            = $request->num_of_staff;
        $floor->display                                    = $request->specifications_display;
        $floor->floorplan                                  = $request->configuration_display;
        $floor->door_position                              = $request->door_position;
        $percentage                                        = ($request->building_shape) ? $request->building_shape : 50;
        ## if operatory rooms are more than 7 then make it 7. it hardcode as ty says client will not add rooms more than 7
        if ($floor->operatory_rooms > 7) {
            $floor->operatory_rooms = 7;
        } elseif ($floor->operatory_rooms < 3) {
            $floor->operatory_rooms = 3;
        }
        if (($floor->operatory_rooms == 3 || $floor->operatory_rooms == 4) && $floor->door_position < 50) {
            $floor->door_position = 50;
        }

        $floorLengthTemp = 520;

        if ($floor->frontdoor_facing == 'east' || $floor->frontdoor_facing == 'west') {
            ## if door at east and west side.
            if ($floor->operatory_rooms <= 3) {
                $footPrintHeight = $floorLengthTemp;
                $footPrintWidth  = 300;
            } else {
                $footPrintWidth  = (100) * ($floor->operatory_rooms);
                $footPrintHeight = $floorLengthTemp;
            }
        } else {
            ## if door at north and south side.
            if ($floor->operatory_rooms <= 3) {
                $footPrintWidth  = 300;
                $footPrintHeight = $floorLengthTemp;
            } else {
                $footPrintWidth  = (100) * ($floor->operatory_rooms);
                $footPrintHeight = $floorLengthTemp;
            }
        }

        if ($floor->operatory_rooms == 6) {
            $footPrintHeight = 490;
        } elseif ($floor->operatory_rooms == 5) {
            $footPrintHeight = 440;
        } elseif ($floor->operatory_rooms == 4) {
            $footPrintHeight = 400;
        } elseif ($floor->operatory_rooms == 3) {
            $footPrintHeight = 300;
        }


        /*
        ## Before client ask, I'll not have more than 7 operatory rooms.
         $horizontalRoomPercentage = 0;
        $minimumFootPrintHeight   = 0;
        $minimumFootPrintWidth    = 0;
        switch ($percentage) {
            case 70:
                $horizontalRoomPercentage = 0.3;
                $minimumFootPrintHeight   = 700;
                $minimumFootPrintWidth    = 400;
                break;
            case 60:
                $horizontalRoomPercentage = 0.4;
                $minimumFootPrintHeight   = 610;
                $minimumFootPrintWidth    = 500;
                break;
            case 50:
                $horizontalRoomPercentage = 0.5;
                $minimumFootPrintHeight   = 610;
                $minimumFootPrintWidth    = 600;
                if ($floor->operatory_rooms == 7) {
                    $minimumFootPrintWidth = 700;
                }
                break;
            case 40:
                $horizontalRoomPercentage = 0.6;
                $minimumFootPrintHeight   = 610;
                $minimumFootPrintWidth    = 700;
                break;
            case 30:
                $horizontalRoomPercentage = 0.7;
                $minimumFootPrintHeight   = 610;
                $minimumFootPrintWidth    = 800;
                break;
        }
        $horizontalRoom  = (int)floor($horizontalRoomPercentage * $floor->operatory_rooms);
        $footPrintWidth  = ($horizontalRoom * self::operatoryRoomWidth);
        $verticalRoom    = $floor->operatory_rooms - $horizontalRoom;
        $footPrintHeight = $verticalRoom * self::operatoryRoomWidth + 200;
        // Minimum foot print width and height
        $footPrintHeight = $footPrintHeight < $minimumFootPrintHeight ? $minimumFootPrintHeight : $footPrintHeight;
        $footPrintWidth  = $footPrintWidth < $minimumFootPrintWidth ? $minimumFootPrintWidth : $footPrintWidth;
        */ ## before Kevin Black.
        //        $length = ($percentage / 100) * $request->square_footage;
        //        $width  = ($request->square_footage - $length);
        //        $floor->length = $length;
        //        $floor->width  = $width;
        $floor->length = $footPrintHeight;
        $floor->width  = $footPrintWidth;
        //        dump($floor->frontdoor_facing);
        //        dump($footPrintWidth);
        //        dump($footPrintHeight);
        //        dump($request->request);
        //        dd($floor);
        $floor->save();

        return redirect()->back()->withSuccess('floor added');
    }

    /**
     * @param $floor
     *
     * @Purpose: Find available Space in the FootPrint...
     * @return array
     */
    public function findAvailableSpaceAtFootPrint(Floor $floor)
    {
        $floor_width                 = $floor->width;
        $floor_length                = $floor->length;
        $number_of_operatoryRoom     = $floor->operatory_rooms;
        $showing_room_on_second_wall = false;
        // There will be always 1 Operatory room so, subtract length of the footprint with 115, as 115 is the height of the operatory room
        // 10 for the borders of the layers.
        $floor_length = $floor_length - $this::operatoryRoomLength - $this->horizontalHallway - 4;


        //        if ($this::operatoryRoomWidth * $number_of_operatoryRoom > $floor_width) {
        if ($this->operatoryRoomsAtTwoWalls) {
            $floor_width                 = $floor_width - $this::operatoryRoomWidth;
            $showing_room_on_second_wall = true;
            //            $this->operatoryRoomsAtTwoWalls = true;
        }
        // Finding x, y axis of the available space
        switch ($floor->frontdoor_facing) {
            case 'north':
                $y_axis = 0;
                $x_axis = 0;
                break;
            case 'east':
                $y_axis = $floor->length - $floor_length;
                $x_axis = $showing_room_on_second_wall ? $this::operatoryRoomWidth : 0;
                break;
            case 'south':
                $y_axis = $floor->length - $floor_length;
                $x_axis = 0;
                break;
            case 'west':
                $y_axis = $floor->length - $floor_length;
                $x_axis = 0;
                break;
            default:
                $y_axis = 0;
                $x_axis = 0;
                break;
        }
        //        $y_axis = $y_axis > 0 ? $y_axis : $y_axis;
        $specification                                    = [
            'x_axis'       => $x_axis.' x-axis',
            'y_axis'       => $y_axis.'  y-axis',
            'floor_width'  => $floor_width.'  width',
            'floor_length' => $floor_length.' length',
        ];
        $this->layersDimensionsAndSize['available_space'] = $specification;

        return $specification;
    }

    /**
     * @param Floor $floor
     *
     * @Purpose: Find the number of Operatory rooms placement at walls & on which wall
     * @return array
     */
    public function findOperatoryRoomPositionAndNumber(Floor $floor)
    {
        $offsetLeft     = 0;
        $offsetTop      = $this->verticalHallway;
        $horizontalRoom = 0;
        $verticalRooms  = 0;
        $operatoryRooms = $floor->operatory_rooms;
        for ($i = 0; $i < $operatoryRooms; $i++) {
            if ($floor->width >= $offsetLeft + $this::operatoryRoomActualWidth) {
                $horizontalRoom += 1;
                $offsetLeft     += $this::operatoryRoomActualWidth;
            }
        }
        for ($i = 0; $i < ($operatoryRooms - $horizontalRoom); $i++) {
            if ($floor->length >= ($offsetTop + $this::operatoryRoomLength)) {
                $verticalRooms += 1;
                $offsetTop     += $this::operatoryRoomLength;
            }
        }
        //        if ($verticalRooms > 1) {
        //            $this->operatoryRoomsAtTwoWalls = true;
        //        } else {
        //            $this->operatoryRoomsAtTwoWalls = false;
        //        }
        $this->availableRoomsAtFootPrint = $horizontalRoom + $verticalRooms;
        switch ($floor->frontdoor_facing) {
            case 'north':
                $horizontalRoom = 'South '.$horizontalRoom.' with window, Rotate 180 degree';
                $verticalRooms  = 'East '.$verticalRooms.', with window, Rotate 90 degree';
                break;
            case 'east':
                $horizontalRoom = 'North '.$horizontalRoom.' with window';
                $verticalRooms  = 'West '.$verticalRooms.' without window';
                break;
            case 'north':
            case 'south':
                $horizontalRoom = 'North '.$horizontalRoom.' with window';
                $verticalRooms  = 'West '.$verticalRooms.' with window, Rotate 90 degree';
                break;
            case 'west':
                $horizontalRoom = 'North '.$horizontalRoom.' with window';
                $verticalRooms  = 'East '.$verticalRooms.', with window, Rotate 90 degree';
                break;
            default:
                break;
        }

        return [
            'verticalRooms'   => $verticalRooms,
            'horizontalRooms' => $horizontalRoom,
        ];
    }

    /**
     * @param Floor $floor
     *
     * @Purpose: Find out the door x, y axis & width, height
     * @return array
     */
    public function findFrontDoorSpecification(Floor $floor)
    {
        $percentage     = $floor->door_position / 100;
        $y_axis         = $percentage * $floor->length;
        $x_axis         = (($percentage * $floor->width) - 50);
        $layer_rotation = 0;
        switch ($floor->frontdoor_facing) {
            case 'north':
            case 'north':
            case 'south':
                $y_axis = 0;
                break;
            case 'east':
            case 'west':
                $x_axis = 0;
                break;
            default:
                break;
        }
        //        $y_axis = $y_axis > 0 ? '-'.$y_axis : $y_axis;
        $specification                              = [
            'x_axis'         => $x_axis.' x-axis',
            'y_axis'         => $y_axis.' y-axis',
            'width'          => 24 .' width',
            'length'         => 24 .' length',
            'layer_rotation' => $layer_rotation,
        ];
        $this->layersDimensionsAndSize['main_door'] = $specification;

        return $specification;
    }

    /**
     * @param Floor $floor
     *
     * @Purpose: Find the Reception & Appointment Layer Dimension
     * @return array
     */
    //    public function findReceptionAndAppointmentLayerSpecification(Floor $floor)
    //    {
    //        $frontDoor = $this->findFrontDoorSpecification($floor);
    //        //        $percentage     = ($floor->door_position / 100);
    //        //        $door_left_axis = ($percentage) * ($floor->width);
    //
    //        return [
    //            //            'x_axis' => $door_left_axis,
    //            'x_axis' => $frontDoor['x_axis'],
    //            //            'y_axis' => $frontDoor['y_axis'],
    //            'y_axis' => 0,
    //            'width'  => '85 width',
    //            'length' => '85 length',
    //        ];
    //    }

    /**
     * @param Floor $floor
     *
     * @Purpose: Finding Private zone specifications
     * @return array
     */
    public function findPrivateZoneSpecification(Floor $floor)
    {
        $availableSpace = $this->findAvailableSpaceAtFootPrint($floor);
        $floor_width    = (((int)$availableSpace['floor_width']) / 2);
        $floor_length   = (int)$availableSpace['floor_length'];
        $y_axis         = 0;
        $x_axis         = 0;
        $hide           = false;
        switch ($floor->frontdoor_facing) {
            case 'east':
            case 'west':
                $hide = true;
                break;
            case 'south':
                $coordiorSecondHallway = $this->layersDimensionsAndSize['second_corridor_hallway'];
                if ($this->isPublicZoneAtRight) {
                    $x_axis = (int)$coordiorSecondHallway['x_axis'] + $this->verticalHallway;
                } else {
                    $x_axis = 0;
                }

                $floor_width  = $this->horizontalSegmentsInLayer;
                $floor_length = $this->horizontalSegmentsLength;

                $firstHorizontalSegment = $this->layersDimensionsAndSize['first_segment_layer'];
                if ($floor->operatory_rooms == 3 || $floor->operatory_rooms == 4) {
                    $firstHorizontalSegment = $this->layersDimensionsAndSize['first_segment_layer'];
                    $floor_length           = $firstHorizontalSegment['length'];
                    $y_axis                 = (int)$firstHorizontalSegment['y_axis'];
                }
                $y_axis = ((int)$firstHorizontalSegment['y_axis']);
                break;
            case 'north':
                $coordiorSecondHallway = $this->layersDimensionsAndSize['second_corridor_hallway'];

                if ($this->isPublicZoneAtRight) {
                    $x_axis = (int)$coordiorSecondHallway['x_axis'] + $this->verticalHallway;
                } else {
                    $x_axis = 0;
                }
                $firstHorizontalSegment = $this->layersDimensionsAndSize['first_segment_layer'];
                $y_axis                 = ((int)$firstHorizontalSegment['y_axis']);
                $floor_width            = $this->horizontalSegmentsInLayer;
                $floor_length           = $this->horizontalSegmentsLength;
                if ($floor->operatory_rooms == 3 || $floor->operatory_rooms == 4) {
                    $firstHorizontalSegment = $this->layersDimensionsAndSize['first_segment_layer'];
                    $floor_length           = $firstHorizontalSegment['length'];
                    $y_axis                 = (int)$firstHorizontalSegment['y_axis'];
                }

                break;
            default:
                break;
        }
        $specification = [
            'width'  => $floor_width.' width',
            'length' => $floor_length.' length',
            'x_axis' => $x_axis.' x-axis',
            'y_axis' => $y_axis.' y-axis',
        ];
        if ($hide) {
            $specification['hide'] = true;
        }
        $this->layersDimensionsAndSize['private_zone'] = $specification;

        return $specification;
    }

    /**
     * @param Floor $floor
     *
     * @Purpose: Find Public zone Dimensions and size
     * @return string[]
     */
    public function findPublicZoneSpecification(Floor $floor)
    {
        $availableSpace = $this->findAvailableSpaceAtFootPrint($floor);
        $floor_width    = (((int)$availableSpace['floor_width']) / 2);
        $floor_length   = ((int)$availableSpace['floor_length']) / 2;
        $door_y_axis    = 0;
        $door_x_axis    = 0;
        $x_axis         = 0;
        $y_axis         = 0;
        $door_picture   = 0;
        $door_rotation  = 0;

        switch ($floor->frontdoor_facing) {
            case 'north':
                $staffContainer = $this->layersDimensionsAndSize['staff_container'];
                $y_axis         = (int)$staffContainer['y_axis'];
                $floor_width    = (((int)$availableSpace['floor_width']) - ((int)$staffContainer['width']));
                $floor_length   = (int)$staffContainer['length'];
                if ($this->isPublicZoneAtRight) {
                    $x_axis = 0;
                } else {
                    $x_axis = (int)$staffContainer['width'];
                }

                $door_picture    = self::doorPictureArray['yellowKnochLeftDown'];
                $corridorHallway = $this->layersDimensionsAndSize['corridor_hallway'];
                $door_x_axis     = (int)$corridorHallway['x_axis'] + 2;
                $door_y_axis     = (int)$staffContainer['length'] - self::doorLength;
                $door_rotation   = 90;
                break;
            case 'east':
                $floor_width  = $this->horizontalSegmentsWidth + 8;
                $floor_length = $this->horizontalSegmentsLength + 4;

                if ($this->isPublicZoneUper) {
                    $x_axis = ($floor->width - $this->horizontalSegmentsWidth - 7);

                    if ($floor->operatory_rooms == 3 || $floor->operatory_rooms == 4) {
                        $y_axis = self::operatoryRoomLength + $this->horizontalHallway;
                    } else {
                        $y_axis = self::operatoryRoomLength + $this->horizontalHallway + 4;
                    }

                } else {
                    $x_axis = ($floor->width - $this->horizontalSegmentsWidth - 4);

                    $staffContainer = $this->layersDimensionsAndSize['staff_container'];
                    $y_axis         = (int)$staffContainer['y_axis'] + (int)$staffContainer['length'];
                    $floor_width    = $this->horizontalSegmentsWidth;

                }

                break;
            case 'south':
                $staffContainer = $this->layersDimensionsAndSize['staff_container'];
                $y_axis         = (int)$staffContainer['y_axis'];
                $floor_width    = (((int)$availableSpace['floor_width']) - ((int)$staffContainer['width']));
                $floor_length   = (int)$staffContainer['length'];
                if ($this->isPublicZoneAtRight) {
                    $x_axis = 0;
                } else {
                    $x_axis = (int)$staffContainer['width'];
                }

                $door_picture    = self::doorPictureArray['yellowKnochDownLeft'];
                $corridorHallway = $this->layersDimensionsAndSize['corridor_hallway'];
                $door_x_axis     = (int)$corridorHallway['x_axis'];
                break;
            case 'west':
                $x_axis       = 0;
                $floor_width  = $this->horizontalSegmentsWidth + 8;
                $floor_length = $this->horizontalSegmentsLength + 4;

                if ($this->isPublicZoneUper) {

                    if ($floor->operatory_rooms == 3 || $floor->operatory_rooms == 4) {
                        $y_axis = self::operatoryRoomLength + $this->horizontalHallway;
                    } else {
                        $y_axis = self::operatoryRoomLength + $this->horizontalHallway + 4;
                    }
                } else {
                    $staffContainer = $this->layersDimensionsAndSize['staff_container'];
                    $y_axis         = (int)$staffContainer['y_axis'] + (int)$staffContainer['length'];
                    $floor_width    = $this->horizontalSegmentsWidth;

                }
                break;
            default:
                break;
        }
        $specification                                = [
            'width'         => $floor_width.' width',
            'length'        => $floor_length.' length',
            'x_axis'        => $x_axis.' x-axis',
            'y_axis'        => $y_axis.' y-axis',
            'door_picture'  => $door_picture,
            'door_x_axis'   => $door_x_axis,
            'door_y_axis'   => $door_y_axis,
            'door_rotation' => $door_rotation,
        ];
        $this->layersDimensionsAndSize['public_zone'] = $specification;

        return $specification;
    }

    /**
     * @param Floor $floor
     *
     * @Purpose: Find Clinical zone Dimensions and size
     * @return string[]
     */
    public function findClinicalZoneSpecification(Floor $floor)
    {
        $availableSpace    = $this->findAvailableSpaceAtFootPrint($floor);
        $doorSpecification = $this->findFrontDoorSpecification($floor);
        $floor_width       = (((int)$availableSpace['floor_width']) / 2);
        $floor_length      = ((int)$availableSpace['floor_length']) / 2;
        $y_axis            = 0;
        $x_axis            = 0;
        $clinicalZoneWidth = $floor_width - $this->verticalHallway;
        switch ($floor->frontdoor_facing) {
            case 'north':
                //                $staffContianer         = $this->layersDimensionsAndSize['staff_container'];
                $firstHorizontalSegment = $this->layersDimensionsAndSize['first_segment_layer'];

                //                $y_axis            = ((int)$staffContianer['length']) + $this->horizontalHallway;
                $y_axis            = ((int)$firstHorizontalSegment['y_axis']);
                $firstCorridor     = $this->layersDimensionsAndSize['corridor_hallway'];
                $clinicalZoneWidth = $this->horizontalSegmentsInLayer;
                $x_axis            = (int)$firstCorridor['x_axis'] + $this->verticalHallway;
                $floor_length      = (int)$this->layersDimensionsAndSize['private_zone']['length'];
                if ($floor->operatory_rooms == 3 || $floor->operatory_rooms == 4) {
                    $firstHorizontalSegment = $this->layersDimensionsAndSize['first_segment_layer'];
                    $floor_length           = $firstHorizontalSegment['length'];
                    $y_axis                 = (int)$firstHorizontalSegment['y_axis'];
                }

                break;
            case 'east':
            case 'west':
            case 'south':
                $y_axis            = $this::operatoryRoomLength + $this->horizontalHallway;
                $firstCorridor     = $this->layersDimensionsAndSize['corridor_hallway'];
                $clinicalZoneWidth = $this->horizontalSegmentsInLayer;
                $x_axis            = (int)$firstCorridor['x_axis'] + $this->verticalHallway;
                $floor_length      = (int)$this->layersDimensionsAndSize['private_zone']['length'];

                if ($floor->frontdoor_facing == 'east' || $floor->frontdoor_facing == 'west') {
                    $floor_length = $this->layersDimensionsAndSize['public_zone']['length'];
                }

                if ($floor->operatory_rooms == 3 || $floor->operatory_rooms == 4) {
                    $firstHorizontalSegment = $this->layersDimensionsAndSize['first_segment_layer'];
                    $floor_length           = $firstHorizontalSegment['length'];
                    $y_axis                 = (int)$firstHorizontalSegment['y_axis'];
                }
                break;
            default:
                break;
        }

        $specification                                  = [
            'width'  => $clinicalZoneWidth.' width',
            'length' => $floor_length.' length',
            'x_axis' => $x_axis.' x-axis',
            'y_axis' => $y_axis.' y-axis',
        ];
        $this->layersDimensionsAndSize['clinical_zone'] = $specification;

        return $specification;
    }

    public function locateDoorOfficeAndToiletContainer()
    {

    }

    public function locateDoorConfluenceAdnMechanicalRoom()
    {

    }

    public function findFirstHorizontalSpecification(Floor $floor)
    {

        $availableSpace = $this->findAvailableSpaceAtFootPrint($floor);

        $y_axis            = 0;
        $x_axis            = 0;
        $clinicalZoneWidth = null;
        $floor_length      = null;
        switch ($floor->frontdoor_facing) {
            case 'west':
                $y_axis            = $this::operatoryRoomLength + $this->horizontalHallway;
                $clinicalZoneWidth = $this->horizontalSegmentsInLayer;
                $x_axis            = $floor->width - $clinicalZoneWidth;

                //                $coordiorSecondHallway = $this->layersDimensionsAndSize['second_corridor_hallway'];
                //                $x_axis                = (int)$coordiorSecondHallway['x_axis'] + $this->verticalHallway;

                $floor_length                   = (int)$availableSpace['floor_length'] - $this->staffContainerLength;
                $this->horizontalSegmentsLength = $floor_length;
                $this->horizontalSegmentsWidth  = $clinicalZoneWidth;
                break;
            case 'north':
                $staffContainer                 = $this->layersDimensionsAndSize['staff_container'];
                $clinicalZoneWidth              = $this->horizontalSegmentsInLayer;
                $floor_length                   = (int)$availableSpace['floor_length'] - $this->staffContainerLength;
                $this->horizontalSegmentsLength = $floor_length;
                $this->horizontalSegmentsWidth  = $clinicalZoneWidth;
                if ($floor->operatory_rooms == 3 || $floor->operatory_rooms == 4) {
                    $y_axis = (int)$staffContainer['length'] + 5;
                } else {
                    $y_axis = (int)$staffContainer['length'] + 3;
                }

                if ($this->isPublicZoneAtRight) {
                    $x_axis = 0;
                } else {
                    $coordiorSecondHallway = $this->layersDimensionsAndSize['second_corridor_hallway'];
                    $x_axis                = (int)$coordiorSecondHallway['x_axis'] + $this->verticalHallway;
                }

                break;
            case 'east':
                $y_axis                         = $this::operatoryRoomLength + $this->horizontalHallway;
                $clinicalZoneWidth              = $this->horizontalSegmentsInLayer;
                $x_axis                         = 0;
                $floor_length                   = (int)$availableSpace['floor_length'] - $this->staffContainerLength;
                $this->horizontalSegmentsLength = $floor_length;
                $this->horizontalSegmentsWidth  = $clinicalZoneWidth;
                break;
            case 'south':
                $y_axis            = $this::operatoryRoomLength + $this->horizontalHallway;
                $clinicalZoneWidth = $this->horizontalSegmentsInLayer;

                if ($this->isPublicZoneAtRight) {
                    $x_axis = 0;
                } else {
                    $coordiorSecondHallway = $this->layersDimensionsAndSize['second_corridor_hallway'];
                    $x_axis                = (int)$coordiorSecondHallway['x_axis'] + $this->verticalHallway;
                }

                $floor_length                   = (int)$availableSpace['floor_length'] - $this->staffContainerLength;
                $this->horizontalSegmentsLength = $floor_length;
                $this->horizontalSegmentsWidth  = $clinicalZoneWidth;
                break;
            default:
                break;
        }
        $specification                                        = [
            'width'  => $clinicalZoneWidth.' width',
            'length' => $floor_length.' length',
            'x_axis' => $x_axis.' x-axis',
            'y_axis' => $y_axis.' y-axis',
            'id'     => 5,
        ];
        $this->layersDimensionsAndSize['first_segment_layer'] = $specification;

        return $specification;
    }

    /**
     * @Purpose Get the Galley Sterilization Dimension
     * @return string[]
     */
    public function galleyStyleSterilization(Floor $floor)
    {
        $width = 80;
        //        $length       = 0;
        //        $id           = 0;
        //        $door_x_axis  = 0;
        //        $door_y_axis  = 0;
        //        $door_picture = 0;

        $clinicalZoneSpecification = $this->layersDimensionsAndSize['clinical_zone'];
        //        $y_axis                    = 0;
        //        $x_axis                    = (int)$clinicalZoneSpecification['width'] - $width;
        $layer_rotation = 0;


        //        if ($this->availableRoomsAtFootPrint > 3 && $this->availableRoomsAtFootPrint < 7) {
        //            $length = 60;
        //            $id     = 17;
        //        } elseif ($this->availableRoomsAtFootPrint == 7 || $this->availableRoomsAtFootPrint == 8) {
        //            $length        = 80;
        //            $id            = 19;
        //            $patientToilet = $this->layersDimensionsAndSize['patient_toilet'];
        //            $x_axis        = $patientToilet['x_axis'];
        //            $y_axis        = $patientToilet['y_axis'];
        //            $width         = (int)$patientToilet['width'];
        //            $length        = (int)$patientToilet['length'];
        //            $door_x_axis   = (int)$patientToilet['door_x_axis'];
        //            $door_y_axis   = (int)$patientToilet['door_y_axis'];
        //            $door_picture  = self::doorPictureArray['darkBlueBottomKnotch'];
        //        } elseif ($this->availableRoomsAtFootPrint > 8) {
        //            $length = 90;
        //            $id     = 18;
        //        }

        $length        = 80;
        $id            = 19;
        $patientToilet = $this->layersDimensionsAndSize['patient_toilet'];
        $x_axis        = $patientToilet['x_axis'];
        $y_axis        = $patientToilet['y_axis'];
        $width         = (int)$patientToilet['width'];
        $length        = (int)$patientToilet['length'];
        $door_x_axis   = (int)$patientToilet['door_x_axis'];
        $door_y_axis   = (int)$patientToilet['door_y_axis'];
        $door_picture  = self::doorPictureArray['darkBlueBottomKnotch'];


        switch ($floor->frontdoor_facing) {
            case 'east':
            case 'west':
                $door_x_axis    = 40;
                $layer_rotation = 90;
                $door_picture   = self::doorPictureArray['blueKnochDownRight'];

                break;
            case 'north':
                $door_x_axis  = 40;
                $door_y_axis  = (int)$length - self::doorLength + 3;
                $door_picture = self::doorPictureArray['blueKnochRightDown'];

                if ($floor->operatory_rooms == 5) {
                    $door_x_axis = 20;
                } elseif ($floor->operatory_rooms == 4) {
                    $door_x_axis = 20;
                    $door_y_axis = $length - self::doorLength + 12;
                } elseif ($floor->operatory_rooms == 3) {
                    $door_x_axis = 20;
                    $door_y_axis = $length - self::doorLength + 13;
                }

                break;
            case 'south':
                $door_x_axis  = 40;
                $door_y_axis  = 0;
                $door_picture = self::doorPictureArray['blueKnochDownRight'];


                if ($floor->operatory_rooms == 5) {
                    $door_x_axis = 20;
                } elseif ($floor->operatory_rooms == 4) {
                    $door_x_axis = 20;
                    $door_y_axis = 0;
                } elseif ($floor->operatory_rooms == 3) {
                    $door_x_axis = 10;
                    $door_y_axis = 0;
                }

                break;
        }

        $specification                                        = [
            'width'          => $width.' Wider',
            'length'         => $length.' Deeper',
            'x_axis'         => $x_axis.' x-axis',
            'y_axis'         => $y_axis.' y-axis',
            'id'             => $id,
            'door_x_axis'    => $door_x_axis,
            'door_y_axis'    => $door_y_axis,
            'door_picture'   => $door_picture,
            'layer_rotation' => $layer_rotation,
            'layer_image'    => self::findBaseUrl('Galley_Sterilization_8x6.png'),

        ];
        $this->layersDimensionsAndSize['galley_sterlization'] = $specification;

        return $specification;
    }

    /**
     * @Purpose: Locate Coffee bar
     * @return array
     */
    public function locateConsultRoom(Floor $floor)
    {
        $coffeeBarWidth = 80;
        //        $coffeeBarWidth  = 30;
        //        $coffeeBarLength = 50;
        $coffeeBarLength = 50;
        $x_axis          = 0;
        $y_axis          = 0;
        $door_x_axis     = 0;
        $door_y_axis     = 0;
        $door_picture    = 0;
        $layer_rotation  = 0;
        $door_rotation   = 0;

        switch ($floor->frontdoor_facing) {
            case 'east':
            case 'west':
                $patientToilet   = $this->layersDimensionsAndSize['patient_toilet'];
                $x_axis          = (int)$patientToilet['x_axis'];
                $y_axis          = (int)$patientToilet['length'] + 4;
                $coffeeBarLength = (int)$patientToilet['length'];
                $coffeeBarWidth  = (int)$patientToilet['width'];
                $door_y_axis     = ($coffeeBarLength - 26);
                $door_x_axis     = ($coffeeBarWidth - self::doorWidth - 5);
                $door_picture    = self::doorPictureArray['grayKnochRightDown'];


                if ($floor->operatory_rooms == 5) {
                    $door_y_axis = ($coffeeBarLength - 19);
                    $door_x_axis = 0;
                } elseif ($floor->operatory_rooms == 4) {
                    $door_y_axis = ($coffeeBarLength - 17);
                    $door_x_axis = 0;
                } elseif ($floor->operatory_rooms == 3) {
                    $door_y_axis = ($coffeeBarLength - 17);
                    $door_x_axis = 0;
                }

                break;
            case 'south':
                $patientToilet   = $this->layersDimensionsAndSize['patient_toilet'];
                $x_axis          = (int)$patientToilet['x_axis'];
                $y_axis          = (int)$patientToilet['length'] + 4;
                $coffeeBarLength = (int)$patientToilet['length'];
                $coffeeBarWidth  = (int)$patientToilet['width'];
                $door_y_axis     = ($coffeeBarLength - self::doorWidth);

                if ($this->isPublicZoneAtRight) {
                    $door_picture = self::doorPictureArray['grayKnochLeftDown'];
                    $door_x_axis  = ($coffeeBarWidth - self::doorWidth);
                } else {
                    $door_picture = self::doorPictureArray['grayKnochRightDown'];
                }

                $y_axis = $y_axis - 3;

                if ($floor->operatory_rooms == 5) {
                    $door_y_axis = ($coffeeBarLength - self::doorLength);
                    $door_x_axis = ($coffeeBarWidth - self::doorWidth);

                } elseif ($floor->operatory_rooms == 4) {
                    $door_y_axis = ($coffeeBarLength - 17);
                    $door_x_axis = ($coffeeBarWidth - 17);
                } elseif ($floor->operatory_rooms == 3) {
                    $door_y_axis = ($coffeeBarLength - 17);
                    $door_x_axis = 0;
                }

                break;
            case 'north':
                $consultRoom     = $this->layersDimensionsAndSize['patient_toilet'];
                $x_axis          = (int)$consultRoom['width'];
                $y_axis          = 0;
                $coffeeBarLength = (int)$consultRoom['length'];
                $coffeeBarWidth  = (int)$consultRoom['width'];


                if ($this->isPublicZoneAtRight) {
                    $layer_rotation = 180;
                    $door_rotation  = 180;
                    $door_y_axis    = ($coffeeBarLength - self::doorWidth);
                    $door_x_axis    = -2;
                    $door_picture   = self::doorPictureArray['grayKnochLeftDown'];
                } else {
                    $door_picture = self::doorPictureArray['grayKnochDownLeft'];
                }

                if ($floor->operatory_rooms == 3) {
                    $door_y_axis = ($coffeeBarLength - 10);
                    $door_x_axis = 0;
                }


                break;
        }
        $specification = [
            'width'          => $coffeeBarWidth.' width',
            'length'         => $coffeeBarLength.' Length',
            'x_axis'         => $x_axis.' x-axis',
            'y_axis'         => $y_axis.' y-axis',
            'door_x_axis'    => $door_x_axis,
            'door_y_axis'    => $door_y_axis,
            'door_picture'   => $door_picture,
            'id'             => 46,
            'layer_rotation' => $layer_rotation,
            'door_rotation'  => $door_rotation,
        ];
        //        dd($specification);
        $this->layersDimensionsAndSize['consult_room'] = $specification;

        return $specification;
    }

    public function labSpecification(Floor $floor)
    {
        $patientToilet  = $this->layersDimensionsAndSize['patient_toilet'];
        $x_axis         = (int)$patientToilet['x_axis'];
        $y_axis         = (int)$patientToilet['length'] + 4;
        $length         = (int)$patientToilet['length'];
        $width          = (int)$patientToilet['width'];
        $door_y_axis    = ($length - 26);
        $door_x_axis    = ($width - self::doorWidth);
        $layer_rotation = 0;
        $door_picture   = self::doorPictureArray['darkBlueUpKnotch'];
        $door_rotation  = 0;
        switch ($floor->frontdoor_facing) {
            case 'east':
            case 'west':

                $consult_room   = $this->layersDimensionsAndSize['consult_room'];
                $x_axis         = (int)$consult_room['x_axis'];
                $y_axis         = (int)$consult_room['y_axis'];
                $length         = (int)$consult_room['length'];
                $width          = (int)$consult_room['width'];
                $door_y_axis    = (0);
                $door_x_axis    = (-1);
                $layer_rotation = 180;
                $door_picture   = self::doorPictureArray['blueKnochLeftDown'];
                $door_rotation  = 180;

                break;
            case 'south':
            case 'north':

                $sterilization  = $this->layersDimensionsAndSize['galley_sterlization'];
                $consult_room   = $this->layersDimensionsAndSize['consult_room'];
                $x_axis         = (int)$consult_room['x_axis'];
                $y_axis         = ((int)$consult_room['y_axis']) - 1;
                $length         = (int)$consult_room['length'];
                $width          = (int)$consult_room['width'];
                $door_y_axis    = (0);
                $door_x_axis    = (0);
                $layer_rotation = 180;
                $door_picture   = self::doorPictureArray['blueKnochLeftDown'];
                $door_rotation  = 180;

                break;
        }


        $specification = [
            'width'          => $width.' Wider',
            'length'         => $length.' Deeper',
            'x_axis'         => $x_axis.' x-axis',
            'y_axis'         => $y_axis.' y-axis',
            'door_x_axis'    => $door_x_axis,
            'door_y_axis'    => $door_y_axis,
            'door_picture'   => $door_picture,
            'id'             => 15,
            'layer_rotation' => $layer_rotation,
            'door_rotation'  => $door_rotation,
            'layer_image'    => self::findBaseUrl('Lab.png'),

        ];

        $this->layersDimensionsAndSize['lab'] = $specification;

        return $specification;
    }


    /**
     * @Purpose Find out Straight Line Sterilization
     * @return string[]
     */
    public function straightLineSterilization(Floor $floor)
    {
        $width  = 0;
        $length = 0;
        $id     = 0;
        if ($this->availableRoomsAtFootPrint > 3 && $this->availableRoomsAtFootPrint < 7) {
            $length = 120;
            $width  = 20;
            $id     = 20;
        } elseif ($this->availableRoomsAtFootPrint == 7 || $this->availableRoomsAtFootPrint == 8) {
            $length = 140;
            $width  = 20;
            $id     = 21;
        } elseif ($this->availableRoomsAtFootPrint > 8) {
            $length = 160;
            $width  = 20;
            $id     = 22;
        }
        $clinicalZoneSpecification = $this->layersDimensionsAndSize['clinical_zone'];
        $y_axis                    = 0;
        $x_axis                    = (int)$clinicalZoneSpecification['width'] - $width;
        switch ($floor->frontdoor_facing) {

            case 'north':
                $y_axis = (int)$clinicalZoneSpecification['length'] - $length;
                $x_axis = 0;
                break;
            case 'east':
                $y_axis = 0;
                $x_axis = 0;
                break;
            case 'west':
                if ($this->isPublicZoneUper) {
                    $y_axis = (int)$clinicalZoneSpecification['length'] - $length;
                } else {
                    $x_axis = (int)$clinicalZoneSpecification['width'] - $width;
                }
                break;
        }

        return [
            'width'  => $width.' Wider',
            'length' => $length.' Deeper',
            'x_axis' => $x_axis.' x-axis',
            'y_axis' => $y_axis.' y-axis',
            'id'     => $id,
        ];
    }

    /**
     * @param Floor $floor
     *
     * @Purpose: Choose the Sterilization on base of footprint width
     * @return string[]
     */
    public function chooseSterilization(Floor $floor)
    {
        if ($floor->width > 400) {
            $sterilization           = $this->galleyStyleSterilization($floor);
            $sterilization['name']   = 'Galley Sterilization';
            $sterilization['reason'] = 'Foot Print width is more than 400';
        } else {
            //            $sterilization           = $this->straightLineSterilization($floor);
            $sterilization           = $this->galleyStyleSterilization($floor);
            $sterilization['name']   = 'Straight Line Sterilization';
            $sterilization['reason'] = 'Foot Print width is less than 400';
        }
        if ($this->operatoryRoomsAtTwoWalls) {
            $sterilization           = $this->locateLShapeSterilization($floor);
            $sterilization['name']   = 'L  Shape Sterilization';
            $sterilization['reason'] = 'Operatory rooms are at two walls';
        }

        return $sterilization;
    }

    /**
     * @param Floor $floor
     *
     * @Purpose: Locate Doctor private's office.
     * @return string[]
     */
    public function locateDoctorPrivateOffice(Floor $floor)
    {
        $x_axis = 0;
        $y_axis = 0;
        //        $privateZoneSpecification = $this->layersDimensionsAndSize['private_zone'];

        $door_x_axis    = 0;
        $door_y_axis    = 0;
        $door_picture   = 0;
        $layer_rotation = 0;
        $layer_priority = null;
        if ($floor->number_of_staff > 9) {
            $dr_office_width  = 100;
            $dr_office_length = 140;
            $id               = 37;
            $reason           = 'Doctors are more than 3';
        } else {
            $dr_office_width  = 90;
            $dr_office_length = 120;
            $id               = 39;
            $reason           = 'Doctors are less then 3';
        }
        $door_rotation = 0;

        switch ($floor->frontdoor_facing) {
            case 'west':
                $parent_id                      = 6;
                $dr_office_and_toilet_container = $this->layersDimensionsAndSize['doctor_private_office_and_toilet_container'];
                $y_axis                         = (int)$dr_office_and_toilet_container['length'] - $dr_office_length - 2;
                $x_axis                         = (((int)$dr_office_and_toilet_container['width'])) - $dr_office_width;
                $door_x_axis                    = $dr_office_width - self::doorWidth;
                $door_y_axis                    = -22;
                $door_picture                   = self::doorPictureArray['grayKnochLeftDown'];

                if ($floor->operatory_rooms == 5) {
                    $door_y_axis = 0;
                    $door_x_axis = $dr_office_width - self::doorWidth;
                }
                break;
            case 'east':
                $parent_id                      = 6;
                $dr_office_and_toilet_container = $this->layersDimensionsAndSize['doctor_private_office_and_toilet_container'];
                $y_axis                         = (int)$dr_office_and_toilet_container['length'] - $dr_office_length - 2;
                $x_axis                         = (((int)$dr_office_and_toilet_container['width']) / 2) - $dr_office_width;
                $door_x_axis                    = -15;
                $door_y_axis                    = -20;
                $door_picture                   = self::doorPictureArray['grayKnochLeftDown'];
                $door_rotation                  = 180;

                if ($floor->operatory_rooms == 6) {
                    $door_x_axis = 0;
                    $door_y_axis = -20;
                }
                if ($floor->operatory_rooms == 5) {
                    $door_x_axis      = 12;
                    $dr_office_width  = 90;
                    $dr_office_length = 63;
                    $x_axis           = -12;
                    $y_axis           = 52;
                }

                if ($floor->operatory_rooms == 3 || $floor->operatory_rooms == 4) {
                    $dr_office_width  = 65;
                    $dr_office_length = 35;
                    $x_axis           = -20;
                    $y_axis           = 10;
                    $door_x_axis      = 17;
                    $door_y_axis      = 0;

                    if ($floor->operatory_rooms == 4) {
                        $door_x_axis = $door_x_axis - 10;
                    }
                }

                break;
            case 'north':
                $parent_id        = 1;
                $layer_rotation   = 180;
                $patient_toilet   = $this->layersDimensionsAndSize['patient_toilet'];
                $door_x_axis      = 0;
                $door_y_axis      = -3;
                $door_picture     = self::doorPictureArray['grayKnochDownLeft'];
                $x_axis           = (int)$patient_toilet['x_axis'];
                $y_axis           = (int)$patient_toilet['y_axis'];
                $dr_office_width  = (int)$patient_toilet['width'];
                $dr_office_length = (int)$patient_toilet['length'];
                break;
            case 'south':
                $patient_toilet   = $this->layersDimensionsAndSize['patient_toilet'];
                $door_x_axis      = 0;
                $door_y_axis      = (int)$patient_toilet['door_y_axis'];
                $door_picture     = self::doorPictureArray['grayKnochDownLeft'];
                $x_axis           = (int)$patient_toilet['x_axis'];
                $y_axis           = (int)$patient_toilet['y_axis'];
                $dr_office_width  = (int)$patient_toilet['width'];
                $dr_office_length = (int)$patient_toilet['length'];
                $parent_id        = 1;

                if ($floor->operatory_rooms == 6) {
                    $door_y_axis = 0;
                }
                if ($floor->operatory_rooms == 3 || $floor->operatory_rooms == 4) {

                    $parent_id   = 1;
                    $door_x_axis = 0;
                    $door_y_axis = 0;
                }
                break;
        }
        $specification                                          = [
            'width'          => $dr_office_width.' width',
            'length'         => $dr_office_length.' length',
            'x_axis'         => $x_axis.' x-axis',
            'y_axis'         => $y_axis.' y-axis',
            'id'             => $id,
            'reason'         => $reason,
            'door_x_axis'    => $door_x_axis,
            'door_y_axis'    => $door_y_axis,
            'door_picture'   => $door_picture,
            'parent_id'      => $parent_id,
            'layer_rotation' => $layer_rotation,
            'layer_priority' => $layer_priority,
            'door_rotation'  => $door_rotation,
            'layer_image'    => self::findBaseUrl('Doctor_s_Office_9x12.png'),

        ];
        $this->layersDimensionsAndSize['doctor_private_office'] = $specification;

        return $specification;
    }

    /**
     * @param Floor $floor
     *
     * @Purpose: Locate Doctor's Toilet
     * @return string[]
     */
    public function locateDoctorToilet(Floor $floor)
    {
        $doctorOfficeSpecification = $this->layersDimensionsAndSize['doctor_private_office'];
        $lab                       = $this->layersDimensionsAndSize['lab'];
        $x_axis                    = (int)$doctorOfficeSpecification['x_axis'];
        $y_axis                    = (int)$doctorOfficeSpecification['length'];
        $door_y_axis               = (0);
        $door_x_axis               = (0);
        $doctorToiletWidth         = 70;
        $doctorToiletLength        = 70;
        $door_picture              = 0;
        $layer_rotation            = 0;
        $door_rotation             = 0;
        $layer_priority            = 0;
        switch ($floor->frontdoor_facing) {
            case 'east':
                $parent_id                      = 3;
                $dr_office_and_toilet_container = $this->layersDimensionsAndSize['consult_room'];
                $y_axis                         = (int)$dr_office_and_toilet_container['y_axis'];
                $x_axis                         = 0;
                $doctorToiletWidth              = $dr_office_and_toilet_container['width'];
                $doctorToiletLength             = $dr_office_and_toilet_container['length'];
                $door_x_axis                    = 0;
                $door_y_axis                    = 0;
                $door_picture                   = self::doorPictureArray['grayKnochDownLeft'];
                $layer_rotation                 = 270;

                if ($floor->operatory_rooms == 5 || $floor->operatory_rooms == 4) {
                    if ($floor->operatory_rooms == 4) {
                        $door_y_axis = 15;
                        $door_x_axis = $door_x_axis + 20;
                    }
                }
                break;
            case 'west':
                $parent_id                      = 6;
                $dr_office_and_toilet_container = $this->layersDimensionsAndSize['doctor_private_office_and_toilet_container'];
                $y_axis                         = (int)$dr_office_and_toilet_container['length'] - $doctorToiletLength - 10;
                $x_axis                         = ((int)$dr_office_and_toilet_container['width'] / 2) - $doctorToiletWidth - 30;
                $door_x_axis                    = $doctorToiletLength;
                $door_y_axis                    = -60;
                $door_picture                   = self::doorPictureArray['grayKnochLeftDown'];


                if ($floor->operatory_rooms == 5 || $floor->operatory_rooms == 4) {
                    $door_y_axis = -40;
                    $door_x_axis = $doctorToiletWidth - self::doorWidth + 30;
                }

                break;
            case 'north':
                $parent_id          = 1;
                $x_axis             = (int)$lab['x_axis'];
                $y_axis             = (int)$lab['y_axis'];
                $doctorToiletLength = (int)$lab['length'];
                $doctorToiletWidth  = (int)$lab['width'];

                if ($this->isPublicZoneAtRight) {
                    $door_x_axis    = -4;
                    $door_y_axis    = $doctorToiletWidth - self::doorWidth - 1;
                    $layer_rotation = 270;
                    $door_picture   = self::doorPictureArray['grayKnotchUpLeftOpening'];
                    $door_rotation  = 90;
                } else {
                    $door_picture   = self::doorPictureArray['grayKnotchUpLeftOpening'];
                    $door_rotation  = 90;
                    $layer_rotation = 270;
                    $door_x_axis    = 0;
                    $door_y_axis    = $doctorToiletWidth - self::doorWidth - 1;
                }

                if ($floor->operatory_rooms == 5) {
                    $door_y_axis = $doctorToiletWidth - self::doorWidth + 10;
                } elseif ($floor->operatory_rooms == 4) {
                    $door_x_axis = -10;
                    $door_y_axis = $doctorToiletWidth - self::doorWidth + 17;;
                } elseif ($floor->operatory_rooms == 3) {
                    $door_y_axis = 21;
                    $door_x_axis = 1;
                }

                break;
            case 'south':
                $parent_id          = 1;
                $door_picture       = self::doorPictureArray['grayKnochDownLeft'];
                $x_axis             = (int)$lab['x_axis'];
                $y_axis             = (int)$lab['y_axis'];
                $doctorToiletLength = (int)$lab['length'];
                $doctorToiletWidth  = (int)$lab['width'];

                $door_x_axis    = 0;
                $door_y_axis    = 0;
                $layer_rotation = 0;
                $door_rotation  = 0;
                if ($floor->operatory_rooms == 4) {
                    //                    $door_x_axis    = -12;
                    //                    $door_y_axis    = $doctorToiletWidth - self::doorWidth + 12;
                    //                    $layer_rotation = 180;

                } elseif ($floor->operatory_rooms == 3) {
                    //                    $x_axis             = $x_axis + 3;
                    //                    $doctorToiletLength = $doctorToiletLength - 30;
                    //                    $door_x_axis        = 0;
                    //                    $door_y_axis        = $doctorToiletWidth - self::doorWidth - 2;
                }
                break;
        }
        $specification = [
            'width'          => $doctorToiletWidth.' width',
            'length'         => $doctorToiletLength.' length',
            'x_axis'         => $x_axis.' x-axis',
            'y_axis'         => $y_axis.' y-axis',
            'door_x_axis'    => $door_x_axis,
            'door_y_axis'    => $door_y_axis,
            'door_picture'   => $door_picture,
            'parent_id'      => $parent_id,
            'layer_rotation' => $layer_rotation,
            'door_rotation'  => $door_rotation,
            'layer_priority' => $layer_priority,
            'layer_image'    => self::findBaseUrl('Doctor_s_Toilet.png'),

        ];

        $this->layersDimensionsAndSize['doctor_toilet'] = $specification;

        return $specification;
    }

    public function locateStaffContainer(Floor $floor)
    {
        $secondCorridorhallway      = $this->layersDimensionsAndSize['second_corridor_hallway'];
        $staffContainerWidth        = 200;
        $this->staffContainerLength = 150;


        if ($floor->operatory_rooms == 5) {
            $this->staffContainerLength = 125;
        }

        if ($floor->operatory_rooms == 4) {
            $this->staffContainerLength = 102;
        }
        if ($floor->operatory_rooms == 3) {
            $this->staffContainerLength = 60;
        }
        $staffContainerLength = $this->staffContainerLength;

        //        if ($floor->operatory_rooms > 5) {
        //            $staffContainerLength = 150;
        //        } else {
        //            $staffContainerLength = 120;
        //        }

        $x_axis                    = 0;
        $y_axis                    = 0;
        $this->isPublicZoneAtRight = ($floor->door_position > 50 ? false : true);
        $this->isPublicZoneUper    = ($floor->door_position > 60 ? false : true);
        $layer_rotation            = 0;


        switch ($floor->frontdoor_facing) {
            case 'west':
                $x_axis = 0;
                //                $y_axis              = $floor->length - $staffContainerLength + 1;
                $staffContainerWidth = $this->verticalHallway + $this->horizontalSegmentsInLayer;

                if ($this->isPublicZoneUper) {
                    $y_axis               = $floor->length - $staffContainerLength + 1;
                    $staffContainerLength = $staffContainerLength - 1;
                } else {
                    $y_axis              = self::operatoryRoomLength + $this->verticalHallway;
                    $staffContainerWidth = ($this->verticalHallway + $this->horizontalSegmentsInLayer) - $this->verticalHallway;
                }


                break;
            case 'east':
                $staffContainerWidth = $this->verticalHallway + $this->horizontalSegmentsInLayer;
                if ($this->isPublicZoneUper) {
                    $x_axis               = (int)$secondCorridorhallway['x_axis'] + 3;
                    $y_axis               = $floor->length - $staffContainerLength + 1;
                    $staffContainerLength = $staffContainerLength - 2;
                } else {
                    $x_axis              = (int)$secondCorridorhallway['x_axis'] - 3;
                    $y_axis              = self::operatoryRoomLength + $this->verticalHallway;
                    $staffContainerWidth = ($this->verticalHallway + $this->horizontalSegmentsInLayer) - $this->verticalHallway;
                    $x_axis              = $x_axis + $this->verticalHallway;
                }
                if ($floor->operatory_rooms == 4) {
                    $staffContainerLength = 100;
                }
                if ($floor->door_position > 50) {
                    $layer_rotation = 180;
                }


                //                $layer_rotation = 180;
                break;
            case 'south':
                if ($this->isPublicZoneAtRight) {
                    // if front door at left side.
                    $x_axis = (int)$secondCorridorhallway['x_axis'] + 4;
                } else {
                    // if front door at right side
                    $x_axis = 0;
                }
                $y_axis               = $floor->length - $staffContainerLength + 1;
                $staffContainerWidth  = $this->verticalHallway + $this->horizontalSegmentsInLayer - 3;
                $staffContainerLength = $staffContainerLength - 4;
                break;
            case 'north':
                if ($this->isPublicZoneAtRight) {
                    // if front door at left side.
                    $x_axis = (int)$secondCorridorhallway['x_axis'] + 4;
                } else {
                    // if front door at right side
                    $x_axis = 0;
                }
                $y_axis              = 0;
                $staffContainerWidth = $this->verticalHallway + $this->horizontalSegmentsInLayer - 4;

                break;
        }
        $specification = [
            'width'          => $staffContainerWidth.' width',
            'length'         => $staffContainerLength.' length',
            'x_axis'         => $x_axis.' x-axis',
            'y_axis'         => $y_axis.' y-axis',
            'id'             => 4,
            'reason'         => '',
            'layer_rotation' => $layer_rotation,
        ];

        $this->layersDimensionsAndSize['staff_container'] = $specification;

        return $specification;
    }

    /**
     * @param Floor $floor
     *
     * @Purpose: Staff Lounge
     * @return string[]
     */
    public function locateStaffLounge(Floor $floor)
    {
        // if staff are 6 or fewer then choose 9 x 11 staff lounge neither choose 10 x 15 if staff is greater than 6
        if ($floor->number_of_staff > 6) {
            // 10 x 15 staff lounge
            $staffLoungeWidth  = 100;
            $staffLoungeLength = 150;
            $id                = 41;
            $reason            = 'Staff are more than 6';
        } else {
            // 9 x 11 staff lounge
            $staffLoungeWidth  = 90;
            $staffLoungeLength = 110;
            $id                = 42;
            $reason            = 'Staff are less than 6';
        }
        $layer_rotation = 0;
        $door_x_axis    = 0;
        $door_y_axis    = 0;
        $door_picture   = 0;
        $door_rotation  = 0;
        switch ($floor->frontdoor_facing) {
            case 'east':
                $staffContainer    = $this->layersDimensionsAndSize['staff_container'];
                $staffLoungeWidth  = ((int)$staffContainer['width']);
                $staffLoungeLength = (int)$staffContainer['length'];
                $y_axis            = $x_axis = 0;
                if ($this->isPublicZoneAtRight) {
                    $door_y_axis    = 0;
                    $door_x_axis    = 0;
                    $layer_rotation = 0;
                    $door_picture   = self::doorPictureArray['grayKnochDownLeft'];
                } else {
                    $door_x_axis    = 0;
                    $door_y_axis    = ((int)$staffContainer['length']) - self::doorWidth;
                    $layer_rotation = 0;
                    $door_picture   = self::doorPictureArray['grayKnochRightDown'];

                }

                break;
            case 'west':

                $staffContainer    = $this->layersDimensionsAndSize['staff_container'];
                $staffLoungeWidth  = ((int)$staffContainer['width']);
                $staffLoungeLength = (int)$staffContainer['length'];
                $y_axis            = $x_axis = 0;

                if ($this->isPublicZoneAtRight) {
                    $door_y_axis    = $staffLoungeLength - self::doorLength;
                    $door_x_axis    = 0;
                    $door_picture   = self::doorPictureArray['grayKnochRightDown'];
                    $layer_rotation = 180;
                } else {
                    $door_picture   = self::doorPictureArray['grayKnochRightDown'];
                    $door_y_axis    = $staffLoungeLength - self::doorLength;
                    $door_x_axis    = 0;
                    $layer_rotation = 180;

                }

                break;
            case 'south':
                $staffContainer    = $this->layersDimensionsAndSize['staff_container'];
                $staffLoungeWidth  = ((int)$staffContainer['width']);
                $staffLoungeLength = (int)$staffContainer['length'];
                $y_axis            = $x_axis = 0;
                if ($this->isPublicZoneAtRight) {
                    $layer_rotation = 0;
                    $door_x_axis    = 0;
                    $door_y_axis    = 0;
                    $door_picture   = self::doorPictureArray['grayKnochDownLeft'];
                } else {
                    $layer_rotation = 180;
                    $door_x_axis    = 0;
                    $door_y_axis    = (int)$staffContainer['length'] - self::doorLength;
                    $door_rotation  = 180;
                    $door_picture   = self::doorPictureArray['grayKnochDownRight'];
                }
                break;
            case 'north':

                $staffContainer    = $this->layersDimensionsAndSize['staff_container'];
                $staffLoungeWidth  = ((int)$staffContainer['width']);
                $staffLoungeLength = (int)$staffContainer['length'];
                $y_axis            = $x_axis = 0;

                if ($this->isPublicZoneAtRight) {
                    $layer_rotation = 0;
                    $door_y_axis    = (int)$staffContainer['length'] - self::doorLength;
                    $door_x_axis    = 0;
                    $door_picture   = self::doorPictureArray['grayKnochRightDown'];
                } else {
                    $door_y_axis    = 0;
                    $door_x_axis    = 0;
                    $door_picture   = self::doorPictureArray['grayKnochDownRight'];
                    $layer_rotation = 180;
                }

                if ($floor->operatory_rooms == 4 || $floor->operatory_rooms == 3) {
                    $door_x_axis = (int)$staffContainer['width'] - self::doorWidth + 10;
                    $door_y_axis = 0;
                }

                break;
        }
        $specification                                 = [
            'width'          => $staffLoungeWidth.' width',
            'length'         => $staffLoungeLength.' length',
            'x_axis'         => $x_axis.' x-axis',
            'y_axis'         => $y_axis.' y-axis',
            'id'             => $id,
            'reason'         => $reason,
            'door_x_axis'    => $door_x_axis,
            'door_y_axis'    => $door_y_axis,
            'door_picture'   => $door_picture,
            'layer_rotation' => $layer_rotation,
            'door_rotation'  => $door_rotation,
            'layer_image'    => self::findBaseUrl('Staff_Lounge_9x11.png'),

        ];
        $this->layersDimensionsAndSize['staff_lounge'] = $specification;

        return $specification;
    }

    /**
     * @param Floor $floor
     *
     * @Purpose: Staff Lounge
     * @return string[]
     */
    public function locateStaffToilet(Floor $floor)
    {
        $consult_room = $this->layersDimensionsAndSize['consult_room'];

        $width  = (int)$consult_room['width'].' width';
        $length = (int)$consult_room['length'].' length';
        //        $x_axis       = (int)$consult_room['x_axis'].' x-axis';
        $x_axis         = '0 x-axis';
        $y_axis         = (int)$consult_room['y_axis'].' y-axis';
        $id             = 44;
        $reason         = 'Choose minimum size on base of Space';
        $door_x_axis    = (int)$consult_room['door_x_axis'];
        $door_y_axis    = (int)$consult_room['door_y_axis'];
        $door_picture   = self::doorPictureArray['grayUpKnoch'];
        $parent_id      = 1;
        $door_rotation  = 0;
        $layer_rotation = 0;
        switch ($floor->frontdoor_facing) {
            case 'south':
                $door_x_axis   = 0;
                $door_y_axis   = (int)$length - self::doorLength;
                $door_picture  = self::doorPictureArray['grayKnochDownRight'];
                $door_rotation = 180;
                if ($this->isPublicZoneAtRight) {
                    $layer_rotation = 0;
                } else {
                    $layer_rotation = 270;
                    $door_x_axis    = 0;
                    $door_y_axis    = (int)$width - self::doorWidth;
                    $door_rotation  = 90;
                    $door_picture   = self::doorPictureArray['grayKnotchUpLeftOpening'];
                }
                break;
            case 'east':
                //
                $parent_id      = 6;
                $width          = (int)$consult_room['width'].' width';
                $length         = (int)$consult_room['length'].' length';
                $x_axis         = (int)($width).' x-axis';
                $y_axis         = 0;
                $layer_rotation = 90;
                $door_rotation  = 180;
                $door_x_axis    = 0;
                $door_y_axis    = (int)$length - self::doorLength;
                $door_picture   = self::doorPictureArray['grayKnochDownRight'];
                break;
            case 'west':
                $labSpecification = $this->layersDimensionsAndSize['lab'];
                $parent_id        = 3;
                $width            = (int)$consult_room['width'].' width';
                $length           = (int)$consult_room['length'].' length';
                $x_axis           = 0;
                $y_axis           = $labSpecification['y_axis'];
                $door_x_axis      = 0;
                $door_y_axis      = (int)$length / 2;
                $door_picture     = self::doorPictureArray['grayKnochLeftDown'];
                $door_rotation    = 180;

                break;
            case 'north':
                $door_picture  = self::doorPictureArray['grayKnochLeftDown'];
                $door_rotation = 180;
                if ($this->isPublicZoneAtRight) {
                    $layer_rotation = 90;
                    $door_y_axis    = ((int)$length) - self::doorLength;
                    $door_x_axis    = 0;
                } else {
                    $layer_rotation = 90;
                    $door_x_axis    = 0;
                    $door_y_axis    = ((int)$length) / 2 - (self::doorWidth / 2);
                }
                break;
            default:
                break;
        }
        $specification                                 = [
            'width'          => $width,
            'length'         => $length,
            'x_axis'         => $x_axis,
            'y_axis'         => $y_axis,
            'id'             => $id,
            'reason'         => $reason,
            'door_x_axis'    => $door_x_axis,
            'door_y_axis'    => $door_y_axis,
            'door_picture'   => $door_picture,
            'parent_id'      => $parent_id,
            'door_rotation'  => $door_rotation,
            'layer_rotation' => $layer_rotation,
        ];
        $this->layersDimensionsAndSize['staff_toilet'] = $specification;

        return $specification;

    }

    public function locateCompasLayer(Floor $floor)
    {
        //$trunk_corridor_bulk_storage_space             = $this->layersDimensionsAndSize['trunk_corridor_bulk_storage_space'];
        $specification                                 = [
            'width'  => '70 width',
            'length' => '82 length',
            'x_axis' => '0 x-axis',
            //            'y_axis' => ((int)$trunk_corridor_bulk_storage_space + 10).' y-axis',
            'y_axis' => 0,
            'id'     => 47,
            $door_x_axis = 0,
            $door_y_axis = 0,
            $door_picture = self::doorPictureArray['0degree'],
        ];
        $this->layersDimensionsAndSize['compas_layer'] = $specification;

        return $specification;
    }

    /**
     * @param Floor $floor
     *
     * @Purpose: Locate Bulk Storage
     * @return array
     */
    public function locateBulkStorage(Floor $floor)
    {
        //        $bulkStorageWidth  = 50;
        //        $bulkStorageLength = 50;

        $bulkStorageWidth  = ($this->horizontalSegmentsWidth / 2);
        $bulkStorageLength = ($this->horizontalSegmentsLength / 2) - 4;

        //        $xRaySpecification         = $this->layersDimensionsAndSize['x_ray'];
        //        $privateZoneSpecification  = $this->layersDimensionsAndSize['private_zone'];
        //        $xRaySpecificationYaxis    = (int)$this->layersDimensionsAndSize['x_ray']['y_axis'];
        $x_axis       = 0;
        $y_axis       = 0;
        $door_x_axis  = 0;
        $door_y_axis  = -2;
        $door_picture = self::doorPictureArray['0degree'];
        //        $consultRoomSpecification  = $this->layersDimensionsAndSize['consult_room'];
        //        $clinicalZoneSpecification = $this->layersDimensionsAndSize['clinical_zone'];
        //        if ($xRaySpecificationYaxis === 0) {
        //            $y_axis = (int)$xRaySpecification['length'];
        //        } else {
        //            $y_axis = ($xRaySpecificationYaxis - $bulkStorageLength);
        //        };
        //        if ((int)$xRaySpecification['x_axis'] <> 0) {
        //            $x_axis = (int)$privateZoneSpecification['width'] - $bulkStorageWidth;
        //        }
        $layer_rotation = 0;
        switch ($floor->frontdoor_facing) {
            //            case 'north':
            //                $y_axis = (int)$consultRoomSpecification['length'] + (int)$consultRoomSpecification['y_axis'];
            //                if ( ! $this->isPublicZoneAtRight) {
            //                    $x_axis = (int)$clinicalZoneSpecification['width'] - $bulkStorageWidth;
            //                }
            //                break;
            case 'north':
                //                $layer_rotation = 180;
                $y_axis       = $bulkStorageLength + 6;
                $x_axis       = 0;
                $door_x_axis  = ($bulkStorageWidth / 2) - (self::doorWidth / 2);
                $door_y_axis  = $bulkStorageLength - self::doorLength + 4;
                $door_picture = self::doorPictureArray['grayKnochRightDown'];

                if ($floor->operatory_rooms == 5) {
                    $door_x_axis = 0;
                    $door_y_axis = $bulkStorageLength - self::doorLength + 10;
                } elseif ($floor->operatory_rooms == 4 || $floor->operatory_rooms == 3) {
                    $door_x_axis = $bulkStorageWidth - self::doorLength + 8;
                    $door_y_axis = $bulkStorageLength - self::doorLength + 15;
                }
                break;
            case 'east':
            case 'west':
            case 'south':
                $y_axis       = 0;
                $x_axis       = 0;
                $door_x_axis  = ($bulkStorageWidth / 2) - (self::doorWidth / 2);
                $door_y_axis  = -3;
                $door_picture = self::doorPictureArray['grayKnochDownRight'];
                break;
            //            case 'east':
            //                $y_axis = (int)$consultRoomSpecification['y_axis'] - $bulkStorageLength;
            //                break;
            //            case 'west':
            //                $x_axis = (int)$clinicalZoneSpecification['width'] - $bulkStorageWidth;
            //                $y_axis = (int)$consultRoomSpecification['length'];
            //                break;
        }
        $specification                                                      = [
            'width'          => $bulkStorageWidth.' width',
            'length'         => $bulkStorageLength.' length',
            'x_axis'         => $x_axis.' x-axis',
            'y_axis'         => $y_axis.' y-axis',
            'id'             => 33,
            'door_x_axis'    => $door_x_axis,
            'door_y_axis'    => $door_y_axis,
            'door_picture'   => $door_picture,
            'layer_image'    => self::findBaseUrl('Trunk_Corridor_Bulk_Storage_Space.png'),
            'layer_rotation' => $layer_rotation,
        ];
        $this->layersDimensionsAndSize['trunk_corridor_bulk_storage_space'] = $specification;

        return $specification;
    }

    /**
     * @param Floor $floor
     *
     * @return array
     */
    public function dentalMechanical(Floor $floor)
    {
        $consultRoomWidth  = 90;
        $consultRoomLength = 90;
        $y_axis            = 0;
        $x_axis            = 0;
        $door_y_axis       = 0;
        $door_x_axis       = 0;
        $door_picture      = null;
        $layer_rotation    = 0;
        switch ($floor->frontdoor_facing) {
            case 'north':
                $layer_rotation                    = 180;
                $trunk_corridor_bulk_storage_space = $this->layersDimensionsAndSize['trunk_corridor_bulk_storage_space'];
                $consultRoomWidth                  = (int)$trunk_corridor_bulk_storage_space['width'];
                $consultRoomLength                 = (int)$trunk_corridor_bulk_storage_space['length'];
                $door_y_axis                       = -2;
                $door_x_axis                       = ($consultRoomWidth / 2) - (self::doorWidth / 2);
                $door_picture                      = self::doorPictureArray['grayKnochDownRight'];
                $x_axis                            = 0;
                $y_axis                            = 0;

                if ($floor->operatory_rooms == 4) {
                    $door_x_axis = $consultRoomWidth - self::doorLength + 10;
                    $door_y_axis = -5;
                }
                break;
            case 'east':
            case 'west':
            case 'south':
                $trunk_corridor_bulk_storage_space = $this->layersDimensionsAndSize['trunk_corridor_bulk_storage_space'];
                $consultRoomWidth                  = (int)$trunk_corridor_bulk_storage_space['width'];
                $consultRoomLength                 = (int)$trunk_corridor_bulk_storage_space['length'];
                $y_axis                            = ((int)$trunk_corridor_bulk_storage_space['length'] + 8);
                $door_y_axis                       = -2;
                $door_x_axis                       = ($consultRoomWidth / 2) - (self::doorWidth / 2);
                $door_picture                      = self::doorPictureArray['grayKnochDownRight'];

                break;
            //            case 'east':
            //                $x_axis = 0;
            //                $y_axis = (int)$clinicalZone['length'] - $consultRoomLength;
            //                break;
            //            case 'west':
            //                if ( ! $this->isPublicZoneUper) {
            //                    $y_axis = (int)$clinicalZone['length'] - $consultRoomLength;
            //                }
            //                break;
        }
        $specification                                      = [
            'width'          => $consultRoomWidth.' width',
            'length'         => $consultRoomLength.' length',
            'x_axis'         => $x_axis,
            'y_axis'         => $y_axis,
            'door_x_axis'    => $door_x_axis,
            'door_y_axis'    => $door_y_axis,
            'door_picture'   => $door_picture,
            'id'             => 32,
            'layer_rotation' => $layer_rotation,
        ];
        $this->layersDimensionsAndSize['dental_mechanical'] = $specification;

        return $specification;
    }

    /**
     * @param Floor $floor
     *
     * @Purpose: Locate Patient Toilet
     * @return array
     */
    public function locatePatientToilet(Floor $floor)
    {
        $patientToiletWidth  = 90;
        $patientToiletLength = 90;
        $id                  = 30;
        $x_axis              = 0;
        $y_axis              = 0;
        $door_x_axis         = 0;
        $door_y_axis         = -2;
        $door_picture        = self::doorPictureArray['0degree'];
        $layer_rotation      = 0;

        switch ($floor->frontdoor_facing) {
            case 'west':
                $trunk_corridor_bulk_storage_space = $this->layersDimensionsAndSize['trunk_corridor_bulk_storage_space'];
                $x_axis                            = ((int)$trunk_corridor_bulk_storage_space['width']);
                $y_axis                            = ((int)$trunk_corridor_bulk_storage_space['y_axis']);
                $patientToiletWidth                = ((int)$trunk_corridor_bulk_storage_space['width']);
                $patientToiletLength               = ((int)$trunk_corridor_bulk_storage_space['length'] + 2);
                $door_x_axis                       = $patientToiletWidth - self::doorWidth;
                $door_y_axis                       = -2;
                $door_picture                      = self::doorPictureArray['yellowKnochDownRight'];
                $layer_rotation                    = 0;
                break;
            case 'east':

                $trunk_corridor_bulk_storage_space = $this->layersDimensionsAndSize['trunk_corridor_bulk_storage_space'];
                $x_axis                            = ((int)$trunk_corridor_bulk_storage_space['width']);
                $y_axis                            = ((int)$trunk_corridor_bulk_storage_space['y_axis']);
                $patientToiletWidth                = ((int)$trunk_corridor_bulk_storage_space['width']);
                $patientToiletLength               = ((int)$trunk_corridor_bulk_storage_space['length'] + 2);
                $door_x_axis                       = $patientToiletWidth - self::doorWidth;
                $door_picture                      = self::doorPictureArray['yellowKnochDownLeft'];
                $layer_rotation                    = 90;
                $door_y_axis                       = 10;

                if ($floor->operatory_rooms == 7) {
                    $door_y_axis = -2;
                } elseif ($floor->operatory_rooms == 5) {
                    $door_y_axis = 14;
                } elseif ($floor->operatory_rooms == 4) {
                    $door_y_axis = 22;
                } elseif ($floor->operatory_rooms == 3 || $floor->operatory_rooms == 4) {
                    $door_y_axis = 20;
                    $door_x_axis = 0;
                }
                break;
            case 'north':
                $trunk_corridor_bulk_storage_space = $this->layersDimensionsAndSize['trunk_corridor_bulk_storage_space'];
                $x_axis                            = ((int)$trunk_corridor_bulk_storage_space['width']);
                $y_axis                            = ((int)$trunk_corridor_bulk_storage_space['y_axis']);
                $patientToiletWidth                = ((int)$trunk_corridor_bulk_storage_space['width']);
                $patientToiletLength               = ((int)$trunk_corridor_bulk_storage_space['length'] + 2);
                $door_x_axis                       = $patientToiletWidth - self::doorWidth + 4;
                $door_picture                      = self::doorPictureArray['yellowKnochLeftDown'];
                $layer_rotation                    = 90;
                $door_y_axis                       = 10;

                //                if ($floor->operatory_rooms == 7) {
                //                    //                    $door_y_axis = 1;
                //                } elseif ($floor->operatory_rooms == 6) {
                //                    //                    $door_y_axis = 8;
                //                    //                    $door_x_axis = $door_x_axis + 9;
                //                } elseif ($floor->operatory_rooms == 5) {
                //                    $door_y_axis = 12;
                //                    $door_x_axis = $door_x_axis + 16;
                //                } elseif ($floor->operatory_rooms == 4) {
                //                    $door_y_axis = 12;
                //                    $door_x_axis = $door_x_axis + 16;
                //                } elseif ($floor->operatory_rooms == 3) {
                //                    $door_y_axis = 20;
                //                    $door_x_axis = $door_x_axis + 22;
                //                }

                break;
            case 'south':
                $trunk_corridor_bulk_storage_space = $this->layersDimensionsAndSize['trunk_corridor_bulk_storage_space'];
                $x_axis                            = ((int)$trunk_corridor_bulk_storage_space['width']);
                $y_axis                            = ((int)$trunk_corridor_bulk_storage_space['y_axis']);
                $patientToiletWidth                = $this->horizontalSegmentsWidth / 2;
                $patientToiletLength               = $this->horizontalSegmentsLength / 2;
                $door_x_axis                       = $patientToiletWidth - self::doorWidth;
                $door_picture                      = self::doorPictureArray['yellowKnochDownRight'];
                if ($this->isPublicZoneAtRight) {
                    $layer_rotation = 90;
                }

                if ($floor->operatory_rooms == 5) {
                    $door_y_axis = 0;
                }

                if ($floor->operatory_rooms == 4) {
                    $door_y_axis = self::doorWidth / 2;
                }


                break;
        }
        $specification                                   = [
            'width'          => $patientToiletWidth.' width',
            'length'         => $patientToiletLength.' length',
            'x_axis'         => $x_axis.' x-axis',
            'y_axis'         => $y_axis.' y-axis',
            'id'             => $id,
            'door_x_axis'    => $door_x_axis,
            'door_y_axis'    => $door_y_axis,
            'door_picture'   => $door_picture,
            'reason'         => 'Choose minimum toilet on base of space',
            'layer_rotation' => $layer_rotation,
        ];
        $this->layersDimensionsAndSize['patient_toilet'] = $specification;

        return $specification;
    }

    /**
     * @Purpose Create Mechanical Room
     *
     * @param Floor $floor
     *
     * @return array
     */
    public function locateMechanicalRoom(Floor $floor)
    {
        //        $mechanicalRoomWidth  = 55;
        //        $mechanicalRoomLength = 55;
        $trunk_corridor_bulk_storage_space = $this->layersDimensionsAndSize['trunk_corridor_bulk_storage_space'];

        $mechanicalRoomWidth  = (int)$trunk_corridor_bulk_storage_space['width'];
        $mechanicalRoomLength = (int)$trunk_corridor_bulk_storage_space['length'];
        $id                   = 45;
        $x_axis               = (int)$trunk_corridor_bulk_storage_space['x_axis'];
        $y_axis               = (int)$trunk_corridor_bulk_storage_space['y_axis'];
        $door_x_axis          = (int)$trunk_corridor_bulk_storage_space['door_x_axis'];
        $door_y_axis          = (int)$trunk_corridor_bulk_storage_space['door_y_axis'];
        $door_picture         = self::doorPictureArray['grayBottomKnoch'];
        $parent_id            = 1;
        $layer_rotation       = 0;
        $door_rotation        = 0;
        switch ($floor->frontdoor_facing) {
            //            case 'north':
            //                $y_axis = (int)$patientToilet['y_axis'] + (int)$patientToilet['length'];
            //                if ($this->isPublicZoneAtRight) {
            //                    $x_axis = (int)$privateZoneSpecification['width'] - $mechanicalRoomWidth;
            //                }
            //                break;
            //            case 'east':
            //                if ($this->isPublicZoneUper) {
            //                    $y_axis = (int)$patientToilet['y_axis'] + (int)$patientToilet['length'];
            //                }
            //                $x_axis = (int)$privateZoneSpecification['width'] - $mechanicalRoomWidth;
            //                break;
            //            case 'west':
            //                if ($this->isPublicZoneUper) {
            //                    $y_axis = (int)$patientToilet['y_axis'] + (int)$patientToilet['length'];
            //                }
            //                break;
            case 'north':
                if ($this->isPublicZoneAtRight) {
                    $door_x_axis   = $mechanicalRoomWidth - self::doorWidth;
                    $door_y_axis   = -3;
                    $door_picture  = self::doorPictureArray['grayKnochRightDown'];
                    $door_rotation = $layer_rotation = 180;
                } else {
                    $door_x_axis    = $mechanicalRoomWidth - self::doorWidth;
                    $door_y_axis    = -2;
                    $door_picture   = self::doorPictureArray['grayKnochRightDown'];
                    $layer_rotation = 180;
                    $door_rotation  = 180;
                }
                break;
            case 'east':
                $y_axis         = 0;
                $x_axis         = 0;
                $parent_id      = 7;
                $door_x_axis    = $mechanicalRoomWidth - self::doorWidth;
                $door_y_axis    = 0;
                $layer_rotation = 0;
                $door_picture   = self::doorPictureArray['grayKnochDownRight'];

                if ($floor->operatory_rooms == 3) {
                    $mechanicalRoomLength = $mechanicalRoomLength - 15;
                }

                break;
            case 'west':
                $y_axis         = 0;
                $x_axis         = 0;
                $parent_id      = 7;
                $door_x_axis    = $mechanicalRoomWidth - self::doorWidth;
                $door_y_axis    = 0;
                $layer_rotation = 0;
                $door_picture   = self::doorPictureArray['grayKnochDownRight'];
                $door_rotation  = 0;
                break;
            case 'south':
                $door_x_axis    = $mechanicalRoomWidth - self::doorWidth;
                $door_y_axis    = 0;
                $door_picture   = self::doorPictureArray['grayKnochDownRight'];
                $layer_rotation = 0;
                break;
        }
        $specification                                    = [
            'width'          => $mechanicalRoomWidth.' width',
            'length'         => $mechanicalRoomLength.' length',
            'x_axis'         => $x_axis.' x-axis',
            'y_axis'         => $y_axis.' y-axis',
            'id'             => $id,
            'reason'         => 'Choose minimum size on base of Space',
            'door_x_axis'    => $door_x_axis,
            'door_y_axis'    => $door_y_axis,
            'door_picture'   => $door_picture,
            'parent_id'      => $parent_id,
            'layer_rotation' => $layer_rotation,
            'door_rotation'  => $door_rotation,
        ];
        $this->layersDimensionsAndSize['mechanical_room'] = $specification;

        return $specification;
    }

    public function locateDoctorOfficeConfluence(Floor $floor)
    {
        $consult_room = $this->layersDimensionsAndSize['consult_room'];

        $width  = (int)$consult_room['width'].' width';
        $length = (int)$consult_room['length'].' length';
        //        $x_axis       = (int)$consult_room['x_axis'].' x-axis';
        $x_axis       = '0 x-axis';
        $y_axis       = (int)$consult_room['y_axis'].' y-axis';
        $id           = 16;
        $reason       = 'Choose minimum size on base of Space';
        $door_x_axis  = (int)$consult_room['door_x_axis'];
        $door_y_axis  = (int)$consult_room['door_y_axis'];
        $door_picture = self::doorPictureArray['grayUpKnoch'];
        $parent_id    = 1;

        $door_rotation = 0;

        switch ($floor->frontdoor_facing) {
            case 'south':
                $door_x_axis   = 0;
                $door_y_axis   = 0;
                $door_picture  = self::doorPictureArray['grayKnochLeftDown'];
                $door_rotation = 180;
                break;
            case 'east':
                //

                $parent_id = 7;
                $width     = (int)$consult_room['width'].' width';
                $length    = (int)$consult_room['length'].' length';
                $x_axis    = (int)($width).' x-axis';
                $y_axis    = 0;

                $door_x_axis  = (int)$consult_room['door_x_axis'];
                $door_y_axis  = -3;
                $door_picture = self::doorPictureArray['grayKnochDownRight'];

                break;
            case 'west':

                $parent_id = 7;
                $width     = (int)$consult_room['width'].' width';
                $length    = (int)$consult_room['length'].' length';
                $x_axis    = (int)($width).' x-axis';
                $y_axis    = 0;

                $door_x_axis  = (int)$consult_room['door_x_axis'];
                $door_y_axis  = 0;
                $door_picture = self::doorPictureArray['grayKnochDownLeft'];

                break;
            case 'north':
                $door_x_axis   = 0;
                $door_y_axis   = 0;
                $door_picture  = self::doorPictureArray['grayKnochLeftDown'];
                $door_rotation = 180;
                break;
            default:
                break;
        }

        $specification                                             = [
            'width'         => $width,
            'length'        => $length,
            'x_axis'        => $x_axis,
            'y_axis'        => $y_axis,
            'id'            => $id,
            'reason'        => $reason,
            'door_x_axis'   => $door_x_axis,
            'door_y_axis'   => $door_y_axis,
            'door_picture'  => $door_picture,
            'parent_id'     => $parent_id,
            'door_rotation' => $door_rotation,
            'layer_image'   => self::findBaseUrl('Dr_s_Office_on_OP_Confluence.png'),
        ];
        $this->layersDimensionsAndSize['doctor_office_confluence'] = $specification;

        return $specification;
    }


    /**
     * @param Floor $floor
     *
     * @Purpose Locate x-ray alcove
     * @return array
     */
    public function locateXRayAlcove(Floor $floor)
    {
        //        if ($floor->cbct) {
        //            $xRayAlcoveWidth  = 80;
        //            $xRayAlcoveLength = 60;
        //            $id               = 35;
        //            $reason           = 'Choose CBCT unit';
        //        } else {
        //            $xRayAlcoveWidth  = 50;
        //            $xRayAlcoveLength = 50;
        //            $id               = 34;
        //            $reason           = "CBCT unit wasn't choose";
        //        }

        $xRayAlcoveWidth  = 80;
        $xRayAlcoveLength = 60;
        $id               = 35;
        $reason           = 'Choose CBCT unit';

        $trunkCooridorBulkStorage = $this->layersDimensionsAndSize['trunk_corridor_bulk_storage_space'];
        $x_axis                   = $trunkCooridorBulkStorage['x_axis'];
        $y_axis                   = $trunkCooridorBulkStorage['y_axis'];
        $xRayAlcoveWidth          = (int)$trunkCooridorBulkStorage['width'];
        $xRayAlcoveLength         = (int)$trunkCooridorBulkStorage['length'];
        //        $doctorOffice = $this->layersDimensionsAndSize['doctor_private_office'];
        //        $privateZone  = $this->layersDimensionsAndSize['private_zone'];
        //        if ((int)$doctorOffice['x_axis'] == 0) {
        //            $x_axis = (int)$privateZone['width'] - $xRayAlcoveWidth;
        //        }
        //        if ((int)$doctorOffice['y_axis'] <> 0) {
        //            $y_axis = (int)$privateZone['length'] - $xRayAlcoveLength;
        //        }
        $door_x_axis    = $trunkCooridorBulkStorage['door_x_axis'];
        $door_y_axis    = $trunkCooridorBulkStorage['door_y_axis'];
        $door_picture   = self::doorPictureArray['darkBlueBottomKnotch'];
        $layer_rotation = 0;
        $door_rotation  = 0;

        switch ($floor->frontdoor_facing) {
            case 'north':
                $layer_rotation = 180;
                $door_rotation  = 180;
                $door_y_axis    = -3;
                $door_x_axis    = ((int)$trunkCooridorBulkStorage['width']) - self::doorWidth;
                $door_picture   = self::doorPictureArray['blueKnochRightDown'];
                if ($floor->operatory_rooms == 6) {
                    $door_y_axis = 0;
                    $door_x_axis = $xRayAlcoveWidth - self::doorWidth;
                } elseif ($floor->operatory_rooms == 5) {
                    $door_x_axis = $xRayAlcoveWidth - self::doorWidth + 8;
                } elseif ($floor->operatory_rooms == 4) {
                    $door_x_axis = $xRayAlcoveWidth - self::doorWidth + 11;
                } elseif ($floor->operatory_rooms == 3) {
                    $door_x_axis = $xRayAlcoveWidth - self::doorWidth + 14;
                }
                break;
            case 'south':
            case 'east':
                $door_x_axis  = ((int)$trunkCooridorBulkStorage['width']) - self::doorWidth;
                $door_picture = self::doorPictureArray['blueKnochDownRight'];
                break;
            case 'west':
                $door_picture = self::doorPictureArray['blueKnochDownRight'];
                break;
        }


        $specification                          = [
            'width'          => $xRayAlcoveWidth.' width',
            'length'         => $xRayAlcoveLength.' Length',
            'x_axis'         => $x_axis.' x-axis',
            'y_axis'         => $y_axis.' y-axis',
            'id'             => $id,
            'reason'         => $reason,
            'door_x_axis'    => $door_x_axis,
            'door_y_axis'    => $door_y_axis,
            'door_picture'   => $door_picture,
            'layer_rotation' => $layer_rotation,
            'door_rotation'  => $door_rotation,
        ];
        $this->layersDimensionsAndSize['x_ray'] = $specification;

        return $specification;
    }

    public function locateBusinessArea(Floor $floor)
    {
        $consult_room   = $this->layersDimensionsAndSize['consult_room'];
        $layer_rotation = 0;
        $door_x_axis    = ((int)$consult_room['width'] - 30);
        $door_y_axis    = ((int)$consult_room['length'] - 30);
        $door_picture   = self::doorPictureArray['darkBlueUpKnotch'];
        $y_axis         = ((int)$consult_room['y_axis']);
        $length         = 145;
        $door_rotation  = 0;
        $width          = (int)$consult_room['width'];
        $parent_id      = 3;
        $x_axis         = 0;
        switch ($floor->frontdoor_facing) {
            case 'east':
                $layer_rotation = 90;
                $parent_id      = 2;

                if ($this->isPublicZoneUper) {
                    $y_axis = -21;
                    $x_axis = 22;
                } else {
                    $y_axis = -21;
                    $x_axis = 19;
                }

                if ($floor->operatory_rooms == 3) {
                    $y_axis = 0;
                }

                break;
            case 'north':
                $door_x_axis    = $length - self::doorLength;
                $door_y_axis    = 0;
                $layer_rotation = 180;
                if ($floor->operatory_rooms == 7) {
                    $y_axis = -41;
                }
                if ($floor->operatory_rooms == 6) {
                    $y_axis = -45;
                }
                if ($floor->operatory_rooms == 5) {
                    $y_axis = -47;
                }
                if ($floor->operatory_rooms == 4) {
                    $y_axis = -31;
                }
                if ($floor->operatory_rooms == 3) {
                    $y_axis = -69;
                }

                break;
            case 'south':
                if ($floor->operatory_rooms == 7) {
                    $y_axis = $y_axis - 3;
                }
                if ($floor->operatory_rooms == 5) {
                    $length = $length - 10;
                    $y_axis = $y_axis - 21;
                }
                if ($floor->operatory_rooms == 6) {
                    $length = $length - 10;
                    $y_axis = $y_axis - 7;
                }
                if ($floor->operatory_rooms == 4) {
                    $y_axis      = $y_axis - 35;
                    $door_y_axis = $length;
                }
                if ($floor->operatory_rooms == 3) {
                    $y_axis      = $y_axis - 45;
                    $door_y_axis = $length;
                }
                $door_rotation = 180;
                break;
            case 'west':
                $layer_rotation = 90;
                $parent_id      = 2;

                if ($this->isPublicZoneUper) {
                    $y_axis = -21;
                    $x_axis = $width - 7;
                } else {
                    $publiczone = $this->layersDimensionsAndSize['public_zone'];
                    $y_axis     = -21;
                    $x_axis     = (int)$publiczone['width'] - $width - 21;
                }
                break;
        }


        $specification                                  = [
            'width'          => $width.' width',
            //            'length'       => (int)$consult_room['length'].' Length',
            'length'         => $length.' Length',
            'x_axis'         => $x_axis.' x-axis',
            'y_axis'         => $y_axis.' y-axis',
            'id'             => 29,
            'door_y_axis'    => $door_y_axis,
            'door_x_axis'    => $door_x_axis,
            //            'door_picture'   => $door_picture,
            'layer_rotation' => $layer_rotation,
            'door_rotation'  => $door_rotation,
            'parent_id'      => $parent_id,
        ];
        $this->layersDimensionsAndSize['business_area'] = $specification;

        return $specification;
    }

    /**
     * @param Floor $floor
     *
     * @Purpose: Locate L Shape Sterilization.
     * @return array
     */
    public function locateLShapeSterilization(Floor $floor)
    {
        $lShapeSterilizationWidth  = 80;
        $lShapeSterilizationLength = 60;
        $clinicalZoneSpecification = $this->layersDimensionsAndSize['clinical_zone'];
        $y_axis                    = 0;
        $x_axis                    = (int)$clinicalZoneSpecification['width'] - $lShapeSterilizationWidth;
        $id                        = 23;
        switch ($floor->frontdoor_facing) {
            case 'east':
            case 'west':
            case 'south':
            case 'north':
                $y_axis = (int)$clinicalZoneSpecification['length'] - $lShapeSterilizationLength;
                $x_axis = 0;
                break;
        }
        $specification                          = [
            'width'  => $lShapeSterilizationWidth.' width',
            'length' => $lShapeSterilizationLength.' Length',
            'x_axis' => $x_axis.' x-axis',
            'y_axis' => $y_axis.' y-axis',
            'id'     => $id,
        ];
        $this->layersDimensionsAndSize['x_ray'] = $specification;

        return $specification;
    }

    /**
     * @param Floor $floor
     *
     * @Purpose: Locate Waiting room..
     * @return array
     */
    public function locateReception(Floor $floor)
    {
        $publicZoneSpecification = $this->layersDimensionsAndSize['public_zone'];
        $waitingRoomWidth        = 208;
        $waitingRoomLength       = 145;

        if ($floor->operatory_rooms == 5) {
            $waitingRoomLength = 114;
        }

        $x_axis         = 0;
        $layer_rotation = 0;
        switch ($floor->frontdoor_facing) {
            case 'east':
                $x_axis = 0;
                $y_axis = (int)$publicZoneSpecification['length'] - $waitingRoomLength;
                if ($floor->operatory_rooms == 6) {
                    $waitingRoomWidth  = $waitingRoomWidth - 50;
                    $waitingRoomLength = $waitingRoomLength - 10;
                    $y_axis            = $y_axis + 10;
                }

                if ($floor->operatory_rooms == 5) {
                    $waitingRoomWidth  = $waitingRoomWidth - 60;
                    $waitingRoomLength = $waitingRoomLength - 15;
                    $y_axis            = $y_axis + 10;
                }
                if ($floor->operatory_rooms == 4) {
                    $waitingRoomWidth  = $waitingRoomWidth - 85;
                    $waitingRoomLength = $waitingRoomLength - 15;
                    $y_axis            = $y_axis + 50;
                }
                if ($floor->operatory_rooms == 3) {
                    $waitingRoomWidth  = 60;
                    $waitingRoomLength = 60;
                    $y_axis            = 0;
                }
                break;
            case 'west':
                $x_axis         = (int)$publicZoneSpecification['width'] - $waitingRoomWidth;
                $y_axis         = (int)$publicZoneSpecification['length'] - $waitingRoomLength;
                $layer_rotation = 180;

                if ($floor->operatory_rooms == 5) {
                    $waitingRoomWidth  = $waitingRoomWidth - 36;
                    $waitingRoomLength = $waitingRoomLength - 30;
                    $x_axis            = $x_axis + 20;
                }
                if ($floor->operatory_rooms == 3 || $floor->operatory_rooms == 4) {
                    $waitingRoomWidth        = 65;
                    $waitingRoomLength       = 80;
                    $publicZoneSpecification = $this->layersDimensionsAndSize['public_zone'];
                    $x_axis                  = (int)$publicZoneSpecification['width'] - $waitingRoomWidth;
                    //                    $y_axis                  = ((int)$publicZoneSpecification['length']) - $waitingRoomLength;
                    $y_axis = 0;
                }

                if ($floor->door_position == 40) {
                    $layer_rotation = 360;
                }
                $x_axis = 0;

                if ($floor->operatory_rooms == 6) {
                    $waitingRoomWidth  = $waitingRoomWidth - 50;
                    $waitingRoomLength = $waitingRoomLength - 10;
                    $y_axis            = $y_axis + 10;
                }

                if ($floor->operatory_rooms == 5) {
                    $waitingRoomWidth  = $waitingRoomWidth - 60;
                    $waitingRoomLength = $waitingRoomLength - 15;
                    $y_axis            = $y_axis + 25;

                }
                if ($floor->operatory_rooms == 4) {
                    $waitingRoomWidth  = 90;
                    $waitingRoomLength = 90;
                    $y_axis            = 80;
                }

                if ($floor->operatory_rooms == 3) {
                    $waitingRoomWidth  = 60;
                    $waitingRoomLength = 60;
                    $y_axis            = 60;
                }

                break;
            case 'south':
                if ($floor->door_position == 60 || $floor->door_position < 30) {
                    $x_axis = (((int)$publicZoneSpecification['width']) - ($waitingRoomWidth)) - 20;
                } elseif ($floor->door_position >= 30) {
                    $x_axis = 0;
                }
                $y_axis = 0;
                if ($floor->operatory_rooms == 3 || $floor->operatory_rooms == 4) {
                    $waitingRoomWidth  = 127;
                    $waitingRoomLength = 86;
                }
                if ($floor->door_position <> 40 && $floor->door_position <> 50) {
                    //                    $layer_rotation = 180;
                    $layer_rotation = 0;
                }
                break;
            case 'north':
                $y_axis = 0;
                if ($floor->door_position == 60 || $floor->door_position < 30) {
                    $layer_rotation = 180;
                    $x_axis         = (((int)$publicZoneSpecification['width']) - ($waitingRoomWidth)) - 10;
                } elseif ($floor->door_position >= 30) {
                    $x_axis = 0;
                }

                if ($floor->operatory_rooms == 3 || $floor->operatory_rooms == 4) {
                    $waitingRoomWidth  = 127;
                    $waitingRoomLength = 86;
                }
                break;
        }

        if ($floor->frontdoor_facing == 'north' || $floor->frontdoor_facing == 'south') {
            //            if ($floor->door_position <> 40 && $floor->door_position <> 50) {
            //                if ($this->isPublicZoneAtRight) {
            //                    $layer_rotation = 270;
            //                } else {
            //                    $layer_rotation = 90;
            //                    //                    $y_axis = $y_axis - 15;
            //                    //                    $x_axis = 10;
            //                }
        } else {
            //                $layer_rotation = 90;
            //            $layer_rotation = 180;
            //            $x_axis         = 20;
        }

        $specification                                 = [
            'width'          => $waitingRoomWidth.' width',
            //            'length' => floor($waitingRoomLength).' Length',
            'length'         => $waitingRoomLength.' Length',
            'x_axis'         => $x_axis.' x-axis',
            'y_axis'         => $y_axis.' y-axis',
            'id'             => 26,
            'layer_rotation' => $layer_rotation,
        ];
        $this->layersDimensionsAndSize['waiting_room'] = $specification;

        return $specification;
    }

    public function locateWaitingRoom(Floor $floor)
    {
        $receptionRoomWidth      = 121;
        $door_specification      = $this->layersDimensionsAndSize['main_door'];
        $publicDoorSpecification = $this->layersDimensionsAndSize['public_zone'];
        $y_axis                  = $publicDoorSpecification['y_axis'];
        $length                  = 100;
        $x_axis                  = ((int)$door_specification['x_axis']) - 12;
        $layer_rotation          = 0;
        switch ($floor->frontdoor_facing) {
            case 'east':
                $x_axis         = ((int)$publicDoorSpecification['x_axis'] + 10);
                $layer_rotation = 180;
                if ($floor->door_position == 60 || $floor->door_position == 90) {
                    $y_axis = (int)$y_axis + self::operatoryRoomLength - 10;
                } else {
                    $y_axis = (int)$y_axis + 10;
                }

                if ($floor->operatory_rooms == 5) {
                    $receptionRoomWidth = $receptionRoomWidth - 30;
                    $length             = $length - 5;
                }

                if ($floor->operatory_rooms == 4) {
                    $receptionRoomWidth = 70;
                    $length             = 60;
                    $y_axis             = ((int)$publicDoorSpecification['y_axis'] + (int)$publicDoorSpecification['length']) - $length;
                }

                if ($floor->operatory_rooms == 3) {
                    $receptionRoomWidth = 60;
                    $length             = 50;
                    $y_axis             = ((int)$publicDoorSpecification['y_axis'] + (int)$publicDoorSpecification['length']) - $length;
                }

                break;
            case 'west':

                $layer_rotation = 0;
                $x_axis         = ((int)$publicDoorSpecification['width']) - $receptionRoomWidth;
                if ($floor->door_position == 60 || $floor->door_position == 90) {
                    $y_axis = (int)$y_axis + self::operatoryRoomLength - 10;
                } else {
                    $y_axis = (int)$y_axis + 10;
                }

                if ($floor->operatory_rooms == 5) {
                    $receptionRoomWidth = $receptionRoomWidth - 30;
                    $length             = $length - 5;
                    $x_axis             = $x_axis + 30;
                    $y_axis             = $y_axis - 10;
                }
                if ($floor->operatory_rooms == 3 || $floor->operatory_rooms == 4) {
                    $receptionRoomWidth = 60;
                    $length             = 60;
                    $y_axis             = $y_axis - 30;
                    $x_axis             = (int)$publicDoorSpecification['width'] - $receptionRoomWidth;
                }

                break;
            case 'north':
                $y_axis             = ((int)$publicDoorSpecification['length'] - $length);
                $receptionRoomWidth = 90;
                $length             = 90;
                $layer_rotation     = 90;

                if ($floor->operatory_rooms == 3 || $floor->operatory_rooms == 4) {
                    $receptionRoomWidth = 70;
                    $length             = 70;
                    $x_axis             = $x_axis + 20;
                    $y_axis             = 20;
                }

                break;
            case 'south':
                $receptionRoomWidth = 90;
                $y_axis             = ((int)$publicDoorSpecification['y_axis']) - 10;
                $length             = 115;
                $x_axis             = ((int)$door_specification['x_axis']) + 6;
                $layer_rotation     = 270;


                if ($floor->operatory_rooms == 3 || $floor->operatory_rooms == 4) {
                    $receptionRoomWidth = 70;
                    $length             = 70;
                    $y_axis             = ((int)$publicDoorSpecification['y_axis']) + 10;

                }

                break;
        }

        $specification = [
            'width'          => $receptionRoomWidth.' width',
            'length'         => $length.' Length',
            'x_axis'         => $x_axis.' axis',
            'y_axis'         => $y_axis.' y axis',
            'layer_rotation' => $layer_rotation,
            'id'             => 14,
            'layer_image'    => self::findBaseUrl('Furniture.png'),
        ];

        $this->layersDimensionsAndSize['waiting_room'] = $specification;

        return $specification;
    }

    public function locateTreatmentHallway(Floor $floor)
    {
        $footPrintSpecification = $this->layersDimensionsAndSize['available_space'];
        $y_axis                 = ((int)$footPrintSpecification['y_axis']) - $this->horizontalHallway;
        $x_axis                 = 0;
        $floor_width            = (int)$footPrintSpecification['floor_width'];
        $floor_length           = $this->horizontalHallway;
        $door_y_axis            = 0;

        if ($floor->operatory_rooms == 4 || $floor->operatory_rooms == 3) {
            $door_y_axis = -14;
        }

        switch ($floor->frontdoor_facing) {
            case 'north':
                $y_axis       = ($floor->length - self::operatoryRoomLength - $this->horizontalHallway);
                $floor_width  = (int)$footPrintSpecification['floor_width'];
                $floor_length = $this->horizontalHallway;
                $door_y_axis  = 7;
                break;
            default:
                break;
        }

        //        dd($y_axis);
        $specification                                      = [
            'width'        => $floor_width.' width',
            'length'       => $floor_length.' length',
            'x_axis'       => $x_axis.' x-axis',
            'y_axis'       => $y_axis.' y-axis',
            'id'           => 50,
            'door_picture' => self::doorPictureArray['whiteDoorOpenBottom'],
            'door_x_axis'  => $floor_width,
            'door_y_axis'  => $door_y_axis,
        ];
        $this->layersDimensionsAndSize['treatment_hallway'] = $specification;

        return $specification;
    }

    public function locateCorridorHallway(Floor $floor)
    {
        $availableSpace = $this->layersDimensionsAndSize['available_space'];
        $floor_width    = $this->verticalHallway;

        switch ($floor->frontdoor_facing) {
            case 'west':
            case 'east':
            case 'north':
            case 'south':
                $layerActualWidth                = (int)$availableSpace['floor_width'] - ($this->verticalHallway * 2);
                $eachSegmentInLayer              = $layerActualWidth / 3;
                $eachSegmentInLayer              = (int)floor($eachSegmentInLayer);
                $this->horizontalSegmentsInLayer = $eachSegmentInLayer;

                $x_axis       = $eachSegmentInLayer;
                $y_axis       = self::operatoryRoomLength + $this->horizontalHallway;
                $floor_length = (((int)$availableSpace['floor_length']) - 150) + 5;
                break;
            default:
                break;
        };

        if ($floor->frontdoor_facing == 'west' || $floor->frontdoor_facing == 'east') {
            $floor_length = $floor->length - 115 - 40;
        }

        $specification = [
            'width'  => $floor_width.' width',
            'length' => $floor_length.' length',
            'x_axis' => $x_axis.' x-axis',
            'y_axis' => $y_axis.' y-axis',
            'id'     => 51,
        ];

        $this->layersDimensionsAndSize['corridor_hallway'] = $specification;

        return $specification;
    }

    public function locateSecondCorridorHallway(Floor $floor)
    {

        $coordiorHallway = $this->layersDimensionsAndSize['corridor_hallway'];
        $floor_width     = $this->verticalHallway;
        switch ($floor->frontdoor_facing) {
            case 'east':
            case 'west':
            case 'north':
            case 'south':
                $x_axis = (int)$coordiorHallway['x_axis'] + $this->horizontalSegmentsInLayer + $this->verticalHallway;

                $y_axis       = (int)$coordiorHallway['y_axis'];
                $floor_length = (int)$coordiorHallway['length'];
                break;
            default:
                break;
        };
        $specification = [
            'width'  => $floor_width.' width',
            'length' => $floor_length.' length',
            'x_axis' => $x_axis.' x-axis',
            'y_axis' => $y_axis.' y-axis',
            'id'     => 52,
        ];

        $this->layersDimensionsAndSize['second_corridor_hallway'] = $specification;

        return $specification;
    }

    private function findDoorOfficeAndToilet(Floor $floor)
    {
        $clinicalZone = $this->layersDimensionsAndSize['clinical_zone'];
        $floor_width  = 0;
        $floor_length = 0;
        $y_axis       = 0;
        $x_axis       = 0;
        $hide         = false;
        switch ($floor->frontdoor_facing) {
            case 'east':
                if ($this->isPublicZoneUper) {

                    $staffContainer = $this->layersDimensionsAndSize['staff_container'];
                    $y_axis         = (int)$staffContainer['y_axis'] + 1;
                    $floor_width    = ((int)$clinicalZone['width']);
                    $floor_length   = (int)$staffContainer['length'] - 5;
                    $x_axis         = $clinicalZone['x_axis'];

                } else {

                    $staffContainer = $this->layersDimensionsAndSize['staff_container'];
                    $y_axis         = (int)$clinicalZone['y_axis'] + (int)$clinicalZone['length'];
                    $floor_width    = ((int)$clinicalZone['width']);
                    $floor_length   = $staffContainer['length'];
                    $x_axis         = $clinicalZone['x_axis'];
                }
                break;
            case 'west':


                if ($this->isPublicZoneUper) {

                    $staffContainer = $this->layersDimensionsAndSize['staff_container'];
                    $y_axis         = (int)$staffContainer['y_axis'] + 1;
                    $floor_width    = ((int)$clinicalZone['width']);
                    $floor_length   = (int)$staffContainer['length'] - 5;
                    $x_axis         = $clinicalZone['x_axis'];

                } else {

                    $staffContainer = $this->layersDimensionsAndSize['staff_container'];
                    $y_axis         = (int)$clinicalZone['y_axis'] + (int)$clinicalZone['length'];
                    $floor_width    = ((int)$clinicalZone['width']);
                    $floor_length   = $staffContainer['length'];
                    $x_axis         = $clinicalZone['x_axis'];
                }

                break;
            case 'north':
            case 'south':
                $hide = true;
                break;
            default:
                break;
        }
        $specification = [
            'width'  => $floor_width.' width',
            'length' => $floor_length.' length',
            'x_axis' => $x_axis.' x-axis',
            'y_axis' => $y_axis.' y-axis',
            'id'     => 6,
        ];

        if ($hide) {
            $specification['hide'] = true;
        }
        $this->layersDimensionsAndSize['doctor_private_office_and_toilet_container'] = $specification;

        return $specification;
    }

    private function findDoorConfluenceAndMechanicalRoom(Floor $floor)
    {
        $clinicalZone = $this->layersDimensionsAndSize['clinical_zone'];
        $floor_width  = 0;
        $floor_length = 0;
        $y_axis       = 0;
        $x_axis       = 0;
        $hide         = false;
        $door_picture = null;
        switch ($floor->frontdoor_facing) {
            case 'west':
                $confluenceHallway = $this->layersDimensionsAndSize['confluence_hallway'];
                $staffContainer    = $this->layersDimensionsAndSize['staff_container'];
                $y_axis            = (int)$confluenceHallway['y_axis'] + (int)$confluenceHallway['length'];
                $x_axis            = ($floor->width - $this->horizontalSegmentsWidth - 2);
                $floor_width       = ((int)$clinicalZone['width']);

                if ($this->isPublicZoneUper) {
                    $floor_length = (int)$staffContainer['length'] - ((int)$confluenceHallway['length']);
                } else {
                    $floor_length = (int)$staffContainer['length'] - ((int)$confluenceHallway['length']);
                }

                break;
            case 'east':
                $confluenceHallway = $this->layersDimensionsAndSize['confluence_hallway'];
                $staffContainer    = $this->layersDimensionsAndSize['staff_container'];
                $y_axis            = (int)$confluenceHallway['y_axis'] + (int)$confluenceHallway['length'];
                $floor_width       = ((int)$clinicalZone['width']);

                if ($this->isPublicZoneUper) {
                    $floor_length = (int)$staffContainer['length'] - 5 - ((int)$confluenceHallway['length']);
                } else {
                    $floor_length = (int)$staffContainer['length'] - ((int)$confluenceHallway['length']);
                }

                break;
            case 'north':
            case 'south':
                $hide = true;
                break;
            default:
                break;
        }
        $specification = [
            'width'        => $floor_width.' width',
            'length'       => $floor_length.' length',
            'x_axis'       => $x_axis.' x-axis',
            'y_axis'       => $y_axis.' y-axis',
            'id'           => 7,
            'door_picture' => $door_picture,
        ];
        if ($hide) {
            $specification['hide'] = true;
        }
        $this->layersDimensionsAndSize['doctor_confluence_and_mechanical_room'] = $specification;

        return $specification;
    }

    private function confluenceHallway(Floor $floor)
    {

        $y_axis               = 0;
        $x_axis               = 0;
        $floor_width          = 0;
        $floor_length         = $this->horizontalHallway;
        $hide                 = true;
        $staffContainerLength = 150;
        switch ($floor->frontdoor_facing) {
            case 'west':
                $clinicalZone = $this->layersDimensionsAndSize['clinical_zone'];
                $y_axis       = (int)$clinicalZone['y_axis'] + (int)$clinicalZone['length'];

                if ($this->isPublicZoneUper) {

                    $secondCorridorhallway = $this->layersDimensionsAndSize['second_corridor_hallway'];
                    //                    if ($this->isPublicZoneAtRight) {
                    //                        $y_axis = $floor->length - $staffContainerLength + 1;
                    //                    } else {
                    //                        $y_axis = 0;
                    //                    }
                    $x_axis      = (int)$secondCorridorhallway['x_axis'] + 4;
                    $floor_width = $this->verticalHallway + $this->horizontalSegmentsInLayer;
                } else {
                    $floor_width = ((int)$clinicalZone['width']);
                    $x_axis      = 0;
                }

                $hide = false;
                break;
            case 'east':
                if ($this->isPublicZoneUper) {
                    $clinicalZone   = $this->layersDimensionsAndSize['clinical_zone'];
                    $staffContainer = $this->layersDimensionsAndSize['staff_container'];
                    $y_axis         = (int)$staffContainer['y_axis'] + 1;
                    $floor_width    = ((int)$clinicalZone['width']) + 4;
                    $x_axis         = 2;
                } else {
                    $clinicalZone = $this->layersDimensionsAndSize['clinical_zone'];
                    $y_axis       = (int)$clinicalZone['y_axis'] + (int)$clinicalZone['length'];
                    $floor_width  = ((int)$clinicalZone['width']);
                    $x_axis       = 0;
                }

                $hide = false;
                break;
            default:
                break;
        }
        $specification = [
            'width'  => $floor_width.' width',
            'length' => $floor_length.' length',
            'x_axis' => $x_axis.' x-axis',
            'y_axis' => $y_axis.' y-axis',
            'id'     => 53,
        ];
        if ($hide) {
            $specification['hide'] = $hide;
        }
        $this->layersDimensionsAndSize['confluence_hallway'] = $specification;

        return $specification;
    }

    public function layersList(Floor $floor)
    {
        $this->findOperatoryRoomPositionAndNumber($floor);
        $this->findFrontDoorSpecification($floor);
        $this->findAvailableSpaceAtFootPrint($floor);
        if ($floor->operatory_rooms == 3 || $floor->operatory_rooms == 4) {
            $this->horizontalHallway = 25;
            $this->verticalHallway   = 25;
        }
        if ($floor->operatory_rooms == 3 || $floor->operatory_rooms == 4) {
            //            $this->horizontalHallway = 30;
            //            $this->verticalHallway   = 30;
        }
        $layers = [
            'treatmentHallway'                  => $this->locateTreatmentHallway($floor),
            'corridorHallway'                   => $this->locateCorridorHallway($floor),
            'secondCorridorHallway'             => $this->locateSecondCorridorHallway($floor),
            'locateStaffContainer'              => $this->locateStaffContainer($floor),
            'firstHorizontalSpace'              => $this->findFirstHorizontalSpecification($floor),
            1                                   => $this->findPrivateZoneSpecification($floor),
            2                                   => $this->findPublicZoneSpecification($floor),
            3                                   => $this->findClinicalZoneSpecification($floor),
            'confluenceHallway'                 => $this->confluenceHallway($floor),
            'dr_office_and_toilet'              => $this->findDoorOfficeAndToilet($floor),
            'dr_confluence_and_mechanical_room' => $this->findDoorConfluenceAndMechanicalRoom($floor),
            'bulk_storage'                      => $this->locateBulkStorage($floor),
            'patient_toilet'                    => $this->locatePatientToilet($floor),
            'consult_room'                      => $this->locateConsultRoom($floor),
            'x_ray_alcove'                      => $this->locateXRayAlcove($floor),
            'sterilization'                     => $this->chooseSterilization($floor),
            'locateBusinessArea'                => $this->locateBusinessArea($floor),
            'lab'                               => $this->labSpecification($floor),
            'mechanical_room'                   => $this->locateMechanicalRoom($floor),
            37                                  => $this->locateDoctorPrivateOffice($floor),
            40                                  => $this->locateDoctorToilet($floor),
            //            'doctorOfficeConflue'               => $this->locateDoctorOfficeConfluence($floor),

            'staffLounge'       => $this->locateStaffLounge($floor),
            //            'waiting_room'      => $this->locateWaitingRoom($floor),
            'receptionRoom'     => $this->locateReception($floor),
            'dental_mechanical' => $this->dentalMechanical($floor),
            'staff_toilet'      => $this->locateStaffToilet($floor),
            //            'locateCompasLayer' => $this->locateCompasLayer($floor),
        ];
        if ($floor->is_coffee_bar) {
            //            $layers['coffee_bar'] = $this->locateCoffeeBar($floor);
        }

        return $layers;
    }

    /**
     * @param Floor $floor
     *
     * @Purpose before saving floor adjust the layers inside the floor...
     */
    public function saveLayers(Floor $floor)
    {
        $layers = $this->layersList($floor);
        // Set all layer to hide....
        DB::table('layer')->update(array('hide' => 1));
        // setting layer's size..
        foreach ($layers as $layerKey => $layerValues) {
            if (isset($layerValues['id'])) {
                $layerId = $layerValues['id'];
            } else {
                $layerId = $layerKey;
            }
            $layer = floorLayers::find($layerId);
            if ($layer instanceof floorLayers) {
                $layer->length = (int)$layerValues['length'];
                $layer->width  = (int)$layerValues['width'];
                if (isset($layerValues['x_axis'])) {
                    $layer->x = (int)$layerValues['x_axis'];
                }
                if (isset($layerValues['y_axis'])) {
                    $layer->y = (int)$layerValues['y_axis'];
                }
                if (isset($layerValues['door_x_axis'])) {
                    $layer->door_x_axis = (int)$layerValues['door_x_axis'];
                }
                if (isset($layerValues['door_y_axis'])) {
                    $layer->door_y_axis = floatval($layerValues['door_y_axis']);
                }
                if (isset($layerValues['door_picture'])) {
                    $layer->door_picture = $layerValues['door_picture'];
                    $layer->show_door    = 1;
                } else {
                    $layer->show_door = 0;
                }
                if ( ! empty($layerValues['parent_id'])) {
                    $layer->parent_id = $layerValues['parent_id'];
                }
                if ( ! empty($layerValues['layer_rotation'])) {
                    $layer->layer_rotation = $layerValues['layer_rotation'];
                } else {
                    $layer->layer_rotation = 0;
                }
                if (isset($layerValues['hide'])) {
                    $layer->hide = 1;
                } else {
                    $layer->hide = 0;
                }
                if (isset($layerValues['priority'])) {
                    $layer->layer_priority = $layerValues['priority'];
                }
                if (isset($layerValues['door_rotation'])) {
                    $layer->door_rotation = $layerValues['door_rotation'];
                }
                //                if (isset($layerValues['layer_image'])) {
                //                    $layer->bg_image = $layerValues['layer_image'];
                //                }

                $layer->save();

            }
        }
    }

    
    ## find coordinator of operatory room of last layer.
    public static function coordinatesOfOpsRoom()
    {

        $lastFloor           = \App\Floor::orderBy('id', 'desc')->first();
        $mainDoor            = \App\floorLayers::getLayerByType('main_door');
        $imageBaseUrl        = \App\Http\Controllers\FloorController::findBaseUrl();
        $height              = $lastFloor->height;
        $width               = $lastFloor->width;
        $offsetLeft          = 0;
        $opsRooms            = [];
        $operatoryRoomWidth  = 100;
        $operatoryRoomHeight = 115;
        for ($x = 0; $x < $lastFloor->operatory_rooms; $x++) {
            $rotate      = 0;
            $offsetTop__ = 0;
            if ($lastFloor->frontdoor_facing === 'north') {
                // if front door at east, then place the room at west walls.
                $offsetTop__ = $height - 115;
                $rotate      = 180;
            }

            $roomSpecification = [
                'offsetLeft' => $offsetLeft,
                'boxWidth'   => $operatoryRoomWidth,
                'boxHeight'  => $operatoryRoomHeight,
                'offsetTop'  => $offsetTop__,
                'imgUrl'     => $imageBaseUrl.'Operatory.png',
                'rotate'     => $rotate,
            ];

            $offsetLeft += $operatoryRoomWidth;
            array_push($opsRooms, $roomSpecification);
        }

        return $opsRooms;

    }

}
