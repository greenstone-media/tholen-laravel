
jQuery(document).ready(function($) {



    /* User filter */

    jQuery("[data-show-filter]").click(function (e) {
        e.preventDefault();
        target = jQuery(jQuery(this).attr('data-show-filter'));
        if(target.length > 0) {
            target.toggleClass("active");
        }
    });

    jQuery("body").css("overflow-x", "hidden");

    /* Uncheck all input checks */
    jQuery("#check-all").change(function () {
        if (!jQuery(this).is(':checked')) {
            jQuery('table tbody').find('input[type=checkbox]').prop("checked", false);
        } else {
            jQuery('table tbody').find('input[type=checkbox]').prop("checked", true);
        }
    });
    jQuery("#checkAll-filter").change(function () {
        if (!jQuery(this).is(':checked')) {
            jQuery('.filter-right').find('input[type=checkbox]').prop("checked", false);
        } else {
            jQuery('.filter-right').find('input[type=checkbox]').prop("checked", true);
        }
    });


    // Add field on number
    if (jQuery('.quantity').length > 0) {
        jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
        jQuery('.quantity').each(function() {
            var spinner = jQuery(this),
                input = spinner.find('input[type="number"]'),
                btnUp = spinner.find('.quantity-up'),
                btnDown = spinner.find('.quantity-down'),
                min = input.attr('min'),
                max = input.attr('max');

            btnUp.click(function() {
                var oldValue = parseFloat(input.val());
                if (oldValue >= max) {
                    var newVal = oldValue;
                } else {
                    var newVal = oldValue + 1;
                }
                spinner.find("input").val(newVal);
                spinner.find("input").trigger("change");
            });

            btnDown.click(function() {
                var oldValue = parseFloat(input.val());
                if (oldValue <= min) {
                    var newVal = oldValue;
                } else {
                    var newVal = oldValue - 1;
                }
                spinner.find("input").val(newVal);
                spinner.find("input").trigger("change");
            });
        });
    }

    jQuery('.wrap-shape input[type="radio"]').click(function(){
        jQuery('.wrap-shape > div').removeClass('active');
        jQuery(this).parent().addClass('active');
    });

});
