@extends('master')
@section('body')
    {{-- Choose Door Position over the wall--}}
    <div class="modal" id="myModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Choose Door Location</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" style="margin-right: 30px">
                    <h2 class="text-center">N</h2>
                    <ul data-wall="north" class="walls-event horizontal-walls walls">
                        <li data-position="10"></li>
                        <li data-position="20"></li>
                        <li data-position="30"></li>
                        <li data-position="40"></li>
                        <li data-position="50"></li>
                        <li data-position="60"></li>
                        <li data-position="70"></li>
                        <li data-position="80"></li>
                        {{--                        <li data-position="90"></li>--}}
                        {{--                        <li data-position="100"></li>--}}
                    </ul>
                    <div style="display:flex;justify-content: space-between">
                        <ul data-wall="west" class="walls-event vertical-walls">
                            {{--                        <li data-position="20"></li>--}}
                            {{--                        <li data-position="30"></li>--}}
                            <li data-position="40"></li>
                            <li data-position="50"><h2 style="margin-left: -100px; color: black">W</h2></li>
                            <li data-position="60"></li>
                            <li data-position="70"></li>
                            <li data-position="80"></li>
                            <li data-position="90"></li>
                        </ul>
                        <ul data-wall="east" class="walls-event vertical-walls">
                            {{--                        <li data-position="20"></li>--}}
                            {{--                        <li data-position="30"></li>--}}
                            <li data-position="40"></li>
                            <li data-position="50"><h2 style="margin-right: -80px; color: black">E</h2></li>
                            <li data-position="60"></li>
                            <li data-position="70"></li>
                            <li data-position="80"></li>
                            <li data-position="90"></li>
                        </ul>
                    </div>
                    <ul data-wall="south" class="walls-event walls horizontal-walls">
                        <li data-position="10"></li>
                        <li data-position="20"></li>
                        <li data-position="30"></li>
                        <li data-position="40"></li>
                        <li data-position="50"></li>
                        <li data-position="60"></li>
                        <li data-position="70"></li>
                        <li data-position="80"></li>
                        {{--                        <li data-position="90"></li>--}}
                        {{--                        <li data-position="100"></li>--}}
                    </ul>
                    <h2 class="text-center">S</h2>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="navigation add-floorplan">
        <div class="sticky">
            <div class="col-12 config-aside ">
                <ul class="nav nav-tabs comon-tabs" id="configTabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="display-tab-link" data-toggle="tab" href="#display_tab"
                           role="tab"
                           aria-controls="home" aria-selected="true">Office Floorplan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="spec-tab-link" data-toggle="tab" href="#rules_tab" role="tab"
                           aria-controls="profile" aria-selected="false">Operations</a>
                    </li>
                </ul>
                @if($errors->all())
                    <br>
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }} <br>
                        @endforeach
                    </div>
                @endif
                @if(session()->has('success'))
                    <br>
                    <div class="alert alert-success">
                        {{session()->get('success')}}!
                    </div>
                @endif
                <form action="{{url('floor_plan')}}" class="form" id="create-floor-form" method="post">
                    {{csrf_field()}}
                    <div class="tab-content" id="myTabContent">
                        {{-- Office Floor Plan Tab--}}
                        <div class="tab-pane fade show active" id="display_tab" role="tabpanel"
                             aria-labelledby="display-tab-link">
                            <div class="row gutter-10">
                                <div class="col-md-6 col-6 form-group">
                                    <label for="Operatory Rooms">Number of Operatory Rooms</label>
                                    <div class="quantity">
                                        <input type="number" id="rooms" name="operatory_rooms" min="3" max="7" step="1"
                                               value="<?php echo $last_floor->operatory_rooms?>" required="required">
                                    </div>
                                </div>
                                <div class="col-md-6 col-6 form-group">
                                    <label for="Square Footage">Total Office Square Footage</label>
                                    <div class="quantity">
                                        <input type="number" id="footage" min="450" name="square_footage"
                                               value="<?php echo $last_floor->square_footage?>"
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="row gutter-10 form-group">
                                {{--                            <div class="col-12">--}}
                                {{--                                <label for="application">Select Building Shape--}}
                                {{--                                    <span>(with North side facing up)</span></label>--}}
                                {{--                                <div class="wrap-shape">--}}
                                {{--                                    <div class="shape-1 <?php echo $last_floor->building_shape == 70 ? 'active' : '' ?>">--}}
                                {{--                                        <input type="radio" name="building_shape"--}}
                                {{--                                               value="70" <?php echo $last_floor->building_shape == 70 ? 'checked="checked"' : '' ?>>--}}
                                {{--                                    </div>--}}
                                {{--                                    <div class="shape-2 <?php echo $last_floor->building_shape == 60 ? 'active' : '' ?>">--}}
                                {{--                                        <input type="radio" name="building_shape"--}}
                                {{--                                               value="60" <?php echo $last_floor->building_shape == 60 ? 'checked="checked"' : '' ?>>--}}
                                {{--                                    </div>--}}
                                {{--                                    <div class="shape-3 <?php echo $last_floor->building_shape == 50 ? 'active' : '' ?>">--}}
                                {{--                                        <input type="radio" name="building_shape"--}}
                                {{--                                               value="50" <?php echo $last_floor->building_shape == 50 ? 'checked="checked"' : '' ?>>--}}
                                {{--                                    </div>--}}
                                {{--                                    <div class="shape-4 <?php echo $last_floor->building_shape == 40 ? 'active' : '' ?>">--}}
                                {{--                                        <input type="radio" name="building_shape"--}}
                                {{--                                               value="40" <?php echo $last_floor->building_shape == 40 ? 'checked="checked"' : '' ?>>--}}
                                {{--                                    </div>--}}
                                {{--                                    <div class="shape-5 <?php echo $last_floor->building_shape == 30 ? 'active' : '' ?>">--}}
                                {{--                                        <input type="radio" name="building_shape"--}}
                                {{--                                               value="30" <?php echo $last_floor->building_shape == 30 ? 'checked="checked"' : '' ?>>--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}
                                {{--                            </div>--}}
                            </div>
                            <div class="row">
                                <div class="gutter-10">
                                    <div class="col-md-12 col-12 form-group">
                                        <label for="display_make">Frontdoor Facing:</label>
                                        <input type="hidden" name="door_position"
                                               value="<?php echo $last_floor->door_position?>">
                                        <input type="hidden" name="frontdoor_facing"
                                               value="<?php echo $last_floor->frontdoor_facing?>">
                                        {{--                                    <select name="frontdoor_facing" id="frontdoor_facing" class="selectpicker">--}}
                                        {{--                                        <option value="" disabled selected>Please select</option>--}}
                                        {{--                                        <option value="north">North</option>--}}
                                        {{--                                        <option value="south">South</option>--}}
                                        {{--                                        <option value="east">East</option>--}}
                                        {{--                                        <option value="west">West</option>--}}
                                        {{--                                    </select>--}}
                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#myModal">
                                            Choose Door
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group-inner brand-checks">
                                <input type="checkbox" name="office_rural" id="office_rural"
                                       onchange="use_outdoors_change(this)" <?php echo $last_floor->rural_area == 1 ? 'checked="checked"' : '' ?>>
                                <label for="office_rural">Is the office in a rural area?</label>
                            </div>
                            <div class="form-group-inner brand-checks">
                                <input type="checkbox" name="radiography_used" id="radiography_used"
                                       onchange="use_outdoors_change(this)" <?php echo $last_floor->cbct == 1 ? 'checked="checked"' : '' ?>>
                                <label for="radiography_used">Will CBCT radiography be used?</label>
                            </div>
                            <div class="form-group-inner brand-checks">
                                <input type="checkbox" name="is_coffe_bar"
                                       id="is_coffe_bar"<?php echo $last_floor->is_coffee_bar == 1 ? 'checked="checked"' : '' ?>>
                                <label for="is_coffe_bar">Do you want a coffee bar? 3x5</label>
                            </div>
                            <div class="form-group-inner brand-checks">
                                <input type="checkbox" name="is_laser_or_larger_equipment"
                                       id="is_laser_or_larger_equipment"<?php echo $last_floor->is_laser_or_large_equipment_use_frequently == 1 ? 'checked="checked"' : '' ?>>
                                <label for="is_laser_or_larger_equipment">Lasers or other large equipment that will be
                                    used
                                    frequently? Vestibule 6x6 </label>
                            </div>
                            <br/>
                            <div class="row gutter-10">
                                <div class="col-md-6 col-6 form-group">
                                    <label for="display_make">Is the office a pediatric or orthodontic office?</label>
                                    <select name="type_office" id="type_office" class="selectpicker">
                                        <option value="" disabled selected>Please select</option>
                                        <option value="pediatric">Pediatric</option>
                                        <option value="orthodontic">Orthodontic</option>
                                    </select>
                                </div>
                                <div class="col-md-6 col-6 form-group">
                                    <label for="display_model">How many people will be at the front desk?</label>
                                    <div class="quantity">
                                        <input type="number" name="front_desk_people" min="1" max="100" step="1"
                                               value="<?php echo $last_floor->front_desk_people?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row gutter-10">
                                <div class="col-md-6 col-6 form-group">
                                    <label for="display_model">How many staff will be present at any one time?</label>
                                    <div class="quantity">
                                        <input type="number" name="num_of_staff" min="1" max="100" step="1"
                                               value="<?php echo $last_floor->number_of_staff?>">
                                    </div>
                                </div>
                            </div>
                            <div class="bottom-controls mx--20">
                                <button type="submit" class="create-floor-btn button btn-red btn-wide">CREATE FLOORPLAN
                                </button>
                            </div>
                        </div>
                        {{-- Rule Tab--}}
                        <div class="tab-pane fade show" id="rules_tab" role="tabpanel"
                             aria-labelledby="display-tab-link">
                            <div class="alert alert-info">
                                All values are in px which is converted by 10xfeet.
                            </div>
                            <p><b>Front Door</b>:
                                @if(isset($doorSpecification))
                                    @foreach( $doorSpecification as $type )
                                        <span>{{ $type }}</span>,
                                    @endforeach
                                @endif
                            </p>
                            <p><b>Footprint Space</b>: {{$last_floor->width}} width, {{$last_floor->length}} length</p>
                            <p><b>Locate Operatory</b>:
                                @if(isset($operatoryRoomsLocation['horizontalRooms']))
                                    {{ $operatoryRoomsLocation['horizontalRooms'] }}
                                @endif
                            </p>
                            {{--                        <p><b>Locate Operatory</b>:--}}
                            {{--                            @if(isset($operatoryRoomsLocation['verticalRooms']))--}}
                            {{--                                {{ $operatoryRoomsLocation['verticalRooms'] }}--}}
                            {{--                            @endif--}}
                            {{--                        </p>--}}
                            <p><b>Available Space</b>:
                                @if(isset($availableSpaceOfFootPrint))
                                    @foreach( $availableSpaceOfFootPrint as $type )
                                        <span>{{ $type }}</span>,
                                    @endforeach
                                @endif
                            </p>
                            <p><b>Size Welcome Room</b>: x, y, width, length, Orientation</p>
                            <p><b>Locate Reception</b>
                                @if(isset($receptionAndAppointmentDesk))
                                    @foreach( $receptionAndAppointmentDesk as $type )
                                        <span>{{ $type }}</span>,
                                    @endforeach
                                @endif
                            </p>
                            <p><b>Private Zone</b>:
                                @if(isset($privateZoneSpecification))
                                    @foreach( $privateZoneSpecification as $type )
                                        <span>{{ $type }}</span>,
                                    @endforeach
                                @endif
                            </p>
                            <p><b>Public Zone</b>:
                                @if(isset($publicZoneSpecification))
                                    @foreach( $publicZoneSpecification as $type )
                                        <span>{{ $type }}</span>,
                                    @endforeach
                                @endif
                            </p>
                            <p><b>Clinical Zone</b>:
                                @if(isset($clinicalZoneSpecification))
                                    @foreach( $clinicalZoneSpecification as $type )
                                        <span>{{ $type }}</span>,
                                    @endforeach
                                @endif
                            </p>
                            <p><b>Treatment Hallway</b>:
                                @if(isset($xHallway))
                                    @foreach( $xHallway as $key => $type )
                                        @if($key <> 'id'  && $key <> 'reason')
                                            <span>{{ $type }}</span>,
                                        @endif
                                    @endforeach
                                @endif
                            </p>
                            <p><b>Trunk Hallway</b>:
                                @if(isset($yHallway))
                                    @foreach( $yHallway as $key => $type )
                                        @if($key <> 'id'  && $key <> 'reason')
                                            <span>{{ $type }}</span>,
                                        @endif
                                    @endforeach
                                @endif
                            </p>
                            <p><b>Patient Toilet</b>: 70 width, 70 length</p>
                            <p><b>Consult Room</b>: 90 width, 90 length</p>
                            <p><b>Pick Sterilization Layer</b>:
                                @if(isset($sterilization['name']))
                                    {{$sterilization['name']}}
                                @endif
                                @if(isset($sterilization['width']))
                                    {{$sterilization['width']}}
                                @endif
                                @if(isset($sterilization['length']))
                                    {{$sterilization['length']}}
                                @endif
                                @if(isset($sterilization['reason']))
                                    <br>
                                    <b>Reason: </b>{{$sterilization['reason']}}
                                @endif
                            </p>
                            <p><b>Pick Dr's Office Layer</b>:
                                @if(isset($doctorPrivateOffice))
                                    @foreach( $doctorPrivateOffice as $key => $type )
                                        @if($key <> 'id'  && $key <> 'reason')
                                            <span>{{ $type }}</span>,
                                        @endif
                                    @endforeach
                                    @if(isset($doctorPrivateOffice['reason']))
                                        <br>
                                        <b>Reason: </b>{{$doctorPrivateOffice['reason']}}
                                    @endif
                                @endif
                            </p>
                            <p><b> Doctor's Toilet</b>:
                                @if(isset($doctorToilet))
                                    @foreach( $doctorToilet as $type )
                                        <span>{{ $type }}</span>,
                                    @endforeach
                                    @if(isset($doctorToilet['reason']))
                                        <br>
                                        <b>Reason: </b>{{$doctorToilet['reason']}}
                                    @endif
                                @endif
                            </p>
                            <p><b>Pick Staff Lounge</b>:
                                @if(isset($staffLounge))
                                    @foreach( $staffLounge as $key => $type )
                                        @if($key <> 'id'  && $key <> 'reason')
                                            <span>{{ $type }}</span>,
                                        @endif
                                    @endforeach
                                    @if(isset($staffLounge['reason']))
                                        <br>
                                        <b>Reason: </b>{{$staffLounge['reason']}}
                                    @endif
                                @endif
                            </p>
                            <p><b>Pick Staff Toilet</b>:
                                @if(isset($staffToilet))
                                    @foreach( $staffToilet as $key => $type )
                                        @if($key <> 'id'  && $key <> 'reason')
                                            <span>{{ $type }}</span>,
                                        @endif
                                    @endforeach
                                    @if(isset($staffToilet['reason']))
                                        <br>
                                        <b>Reason: </b>{{$staffToilet['reason']}}
                                    @endif
                                @endif
                            </p>
                            <p><b>Pick Patient Toilet</b>:
                                @if(isset($patientToilet))
                                    @foreach( $patientToilet as $key => $type )
                                        @if($key <> 'id'  && $key <> 'reason')
                                            <span>{{ $type }}</span>,
                                        @endif
                                    @endforeach
                                    @if(isset($patientToilet['reason']))
                                        <br>
                                        <b>Reason: </b>{{$patientToilet['reason']}}
                                    @endif
                                @endif
                            </p>
                            <p><b>Pick Mechanical Room</b>:
                                @if(isset($mechanicalRoom))
                                    @foreach( $mechanicalRoom as $key => $type )
                                        @if($key <> 'id'  && $key <> 'reason')
                                            <span>{{ $type }}</span>,
                                        @endif
                                    @endforeach
                                    @if(isset($mechanicalRoom['reason']))
                                        <br>
                                        <b>Reason: </b>{{$mechanicalRoom['reason']}}
                                    @endif
                                @endif
                            </p>
                            <p><b>Pick X-Ray </b>:
                                @if(isset($xRay))
                                    @foreach( $xRay as $key => $type )
                                        @if($key <> 'id'  && $key <> 'reason')
                                            <span>{{ $type }}</span>,
                                        @endif
                                    @endforeach
                                    @if(isset($xRay['reason']))
                                        <br>
                                        <b>Reason: </b>{{$xRay['reason']}}
                                    @endif
                                @endif
                            </p>
                            <p><b>Trunk Corridor Bulk Storage Space </b>:
                                @if(isset($storage))
                                    @foreach( $storage as $key => $type )
                                        @if($key <> 'id'  && $key <> 'reason')
                                            <span>{{ $type }}</span>
                                        @endif
                                    @endforeach
                                @endif
                            </p>
                            <p><b>Waiting Room </b>:
                                @if(isset($waitingRoom))
                                    @foreach( $waitingRoom as $key => $type )
                                        @if($key <> 'id'  && $key <> 'reason')
                                            <span>{{ $type }}</span>,
                                        @endif
                                    @endforeach
                                @endif
                            </p>
                            <p><b>Reception Room </b>:
                                @if(isset($receptionRoom))
                                    @foreach( $receptionRoom as $key => $type )
                                        @if($key <> 'id'  && $key <> 'reason')
                                            <span>{{ $type }}</span>,
                                        @endif
                                    @endforeach
                                @endif
                            </p>
                            <p><b>Coffee Bar </b>:
                                @if(isset($coffee_bar))
                                    @foreach( $coffee_bar as $key => $type )
                                        @if($key <> 'id'  && $key <> 'reason')
                                            <span>{{ $type }}</span>,
                                        @endif
                                    @endforeach
                                @endif
                            </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- main-content -->
    <section id="main-content" class="main-content">

        <iframe
                {{--                src="http://api2.makli.com"--}}
                {{--                                                src="{{env('iframe_source_url')}}"--}}
                src="http://localhost:3000"
                style="width:800px; height:100vh; border:0; border-radius: 4px; overflow:hidden;"
                title="distracted-dawn-q60jb"
                allow="geolocation; microphone; camera; midi; vr; accelerometer; gyroscope; payment; ambient-light-sensor; encrypted-media; usb"
                sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"
        ></iframe>
    </section>
@endsection
