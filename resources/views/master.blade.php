<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel='stylesheet' id='fontawesome-css' href='https://use.fontawesome.com/releases/v5.0.1/css/all.css?ver=4.9.1'
          type='text/css' media='all'/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Sintony:400,700&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet " href="{{asset('assets/css/font-awesome.min.css')}}">
    {{--    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">--}}
    <link rel="stylesheet" href="{{asset('assets/css/style-2.css?89.09')}}">
    <link rel="stylesheet " href="{{asset('assets/css/bootstrap-select.min.css')}}">
    <title>Dashboard - Thoolen's</title>
    @yield('styleScript')
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body>
<header id="main-header" class="main-header">
    <nav id="navbar" class="has_fixed_navbar main-nav animate-me" style="top: 0px;">
        <div class="nav-logo">
            <a href="javascript:void(0)" id="nav-trigger"></a>
        </div>
        <div class="nav-center">
            <div id="nav-user" class="nav-user">
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Choose an item
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </div>
            </div>
            <div class="nav-title">
                <h2>Dr Tholen's Office</h2>
            </div>
            <!-- SIDEBAR TOGGLE -->
            <ul>
                <li class="notification dropdown">
                    <a href="javascript:void(0)" id="nav-notification-trigger" title="View your notifications"
                       data-toggle="dropdown" aria-expanded="false">
                        <i class="far fa-bell"></i>
                        <span class="badge badge-warning">4</span>
                    </a>
                </li>
            </ul>
        </div> <!-- /. nav-left -->
        <!-- EXTRA BUTTONS ABOVE THE SIDBAR -->
        <div class="nav-buttons">
            <!-- user info -->
            <div id="nav-user" class="nav-user">
                <img alt="" src="{{asset('assets/img/gravatar.jpg')}}" class="avatar avatar-96 photo" height="35"
                     width="35">
            </div>
            <!-- /. user info -->
        </div>
        <div class="nav-buttons">
            <a class="nav-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </nav>
</header>

@yield('body')
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="{{asset('assets/js/app.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-select.min.js')}}"></script>
@yield('jsScript')

<script>
	$( document ).ready( function() {
		var frontDoorFacingInput = $( 'input[name=frontdoor_facing]' );
		var doorPosition = $( 'input[name=door_position]' );
		// on page load, select the front door position
		if ( $( frontDoorFacingInput ).val() && $( doorPosition ).val() ) {
			$( 'ul.walls-event[data-wall=' + $( frontDoorFacingInput ).val() + ']' ).find( 'li[data-position=' + $( doorPosition ).val() + ']' ).addClass( 'active' ).css( {
				'width': '2.3em',
				'height': '2.3em'
			} );
		}
		// saving floor/footprint
		$( document ).on( 'click', '.create-floor-btn', function() {
			$( '#create-floor-form' ).submit();
		} );
		// saving door position to the input field.
		$( document ).on( 'click', 'ul.walls-event li', function() {
			var walls = $( this ).closest( 'ul' ).attr( 'data-wall' );
			var position = $( this ).attr( 'data-position' );
			$( 'ul.walls-event li' ).removeClass( 'active' ).css( { 'width': '2em', 'height': '2em' } );
			$( this ).addClass( 'active' ).css( { 'width': '2.3em', 'height': '2.3em' } );
			$( frontDoorFacingInput ).val( walls );
			$( doorPosition ).val( position );
		} );
		$( '#rooms' ).on( "change paste keyup", function() {
			$( '#footage' ).val( ($( this ).val() * 500) );
		} );
		$( '#footage' ).on( "change paste keyup", function() {
			$footage_value = ($( this ).val() / 500);
			$footage_value = ~~$footage_value;
			$( '#rooms' ).val( $footage_value );
		} );
	} );
</script>
</body>
</html>
