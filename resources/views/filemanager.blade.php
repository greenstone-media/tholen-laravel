@extends('master')
@section('styleScript')
    <link rel="stylesheet" href="{{ asset('vendor/file-manager/css/file-manager.css') }}">
@endsection
@section('body')
    <br><br><br>
    <div style="height: 600px;">
        <div id="fm"></div>
    </div>
@endsection
@section('jsScript')
    <script src="{{ asset('vendor/file-manager/js/file-manager.js') }}"></script>
@endsection
